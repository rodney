#!/bin/bash
# rodney -- fractal zoom explorer bot
# Copyright (C) 2021 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-or-later
set -e
if [ "x${wide}" = "xtall" ]
then
  if [ "x${hq}" = "x1" ]
  then
    aa=11
    width=1080
    height=1920
    samples=256
  else
    aa=1
    width=180
    height=320
    samples=8
  fi
else
  if [ "x${hq}" = "x1" ]
  then
    aa=11
    width=1920
    height=1080
    samples=256
  else
    aa=1
    width=320
    height=180
    samples=8
  fi
fi
small=160x90
ME="$(dirname "$(readlink -e $0)")"
rodneyexr="${ME}/rodney-exr"
sonify="${ME}/sonify"
zoomasm="${ME}/../zoomasm/zoomasm"
for seed in "$@"
do
  if [ ! -e "${seed}${wide}.mp4" ]
  then
    mkdir -p "${seed}"
    pushd "${seed}"
    if [ ! -e "${seed}-palette.frag" ]
    then
      "${rodneyexr}" -1 --antialiasing "$aa" --render-quality 10 --threads 16 --verbose 3 --seed "${seed}"
    fi
    ( cat "${ME}/palette.frag" "${seed}-palette.frag" | sed "s/nan/0.0/g" && echo && echo "vec3 colour(void) { float49 N = getN(); float T = getT(); vec2 DE = getDE(); float Zoom = getZoomLog2(); return colour(pal, vec4(Zoom), getInterior() ? 0xFFffFFffu : uint(to_int(floor(N))), vec2(to_float(sub(N, floor(N))), T), DE, vec3(0.0)).rgb; }" ) > colour.glsl
    n="$(ls -1 *.exr | wc -l)"
    cat <<EOF > zoomasm.toml
[audio]
duration = ${n}.0
loop = false
mute = false
playing = false
speed = 1.0
time = 0.0
type = 'silence'
volume = 1.0

[colour]
angle = 0.0
colour = 'colour.glsl'
projection = '2D'
samples = ${samples}
shutter = 0.5
watch = true

[input]
channels = [ 'DEX', 'DEY', 'N', 'NF', 'T' ]
flip = false
input = '.'
invert = false
layers = 16
reverse = false
vram_override = false

[output]
advanced = false
audio_kbps = 384.0
ffmpeg = 'ffmpeg'
fps = 30.0
height = ${height}
output = 'output${wide}.mp4'
overwrite = false
pass1 = ''
pass2 = ''
twopass = false
video_crf = 20
width = ${width}

[[timeline.timeline]]
Z = 0.0
interpolation = 'Cubic'
t = 0.0

[[timeline.timeline]]
Z = ${n}.0
interpolation = 'Cubic'
t = 1.0
EOF
    "${zoomasm}" --record zoomasm.toml
    i="output${wide}.mp4"
    w="output${wide}.wav"
    a="output${wide}.m4a"
    o="../${seed}${wide}.mp4"
    ffmpeg -i "${i}" -s "${small}" -f image2pipe pipe:-.ppm |
    "${sonify}" "${w}"
    ecanormalize "${w}"
    fdkaac -b384k "${w}" -o "${a}"
    ffmpeg -i "${i}" -i "${a}" -codec:v copy -codec:a copy -movflags +faststart "${o}"
    popd
  fi
done
