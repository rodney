// rodney -- fractal zoom explorer bot
// Copyright (C) 2020 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "worker.h"

#include <pthread.h>

#include <list>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>

struct workerpool
{
  std::vector<pthread_t> workers;
  pthread_mutex_t mutex;
  pthread_cond_t jobs_available;
  std::list<struct work> jobs;
  std::list<struct work> results;
  worker callback;
  bool running;
};

void workerpool_put(struct workerpool *pool, struct work job)
{
  struct work copy;
  copy.size = job.size;
  copy.data = (char *) std::malloc(copy.size);
  std::memcpy(copy.data, job.data, copy.size);
  pthread_mutex_lock(&pool->mutex);
  pool->jobs.push_back(copy);
  pthread_cond_signal(&pool->jobs_available);
  pthread_mutex_unlock(&pool->mutex);
}

bool workerpool_empty(struct workerpool *pool)
{
  pthread_mutex_lock(&pool->mutex);
  bool empty = pool->results.empty();
  pthread_mutex_unlock(&pool->mutex);
  return empty;
}

struct work workerpool_get(struct workerpool *pool)
{
  pthread_mutex_lock(&pool->mutex);
  struct work result = { 0, 0 };
  if (! pool->results.empty())
  {
    result = pool->results.front();
    pool->results.pop_front();
  }
  pthread_mutex_unlock(&pool->mutex);
  return result;
}

void *workerpool_thread(void *arg)
{
  struct workerpool *pool = (struct workerpool *) arg;
  pthread_mutex_lock(&pool->mutex);
  while (pool->running)
  {
    if (pool->jobs.empty())
    {
      pthread_cond_wait(&pool->jobs_available, &pool->mutex);
    }
    if (pool->running)
    {
      if (! pool->jobs.empty())
      {
        struct work job = pool->jobs.front();
        pool->jobs.pop_front();
        pthread_mutex_unlock(&pool->mutex);
        struct work result = { 0, 0 };
        try
        {
          result = pool->callback(job);
        }
        catch (std::exception &e)
        {
          // ignore
        }
        std::free(job.data);
        pthread_mutex_lock(&pool->mutex);
        pool->results.push_back(result);
      }
    }
  }
  pthread_mutex_unlock(&pool->mutex);
  return 0;
}

struct workerpool *workerpool_new(int threads, worker callback)
{
  struct workerpool *pool = new workerpool();
  pool->running = true;
  pool->callback = callback;
  pthread_mutex_init(&pool->mutex, 0);
  pthread_cond_init(&pool->jobs_available, 0);
  pthread_mutex_lock(&pool->mutex);
  for (int i = 0; i < threads; ++i)
  {
    pthread_t thread;
    if (0 == pthread_create(&thread, 0, workerpool_thread, pool))
    {
      pool->workers.push_back(thread);
    }
    else
    {
      // error
    }
  }
  pthread_mutex_unlock(&pool->mutex);
  return pool;
}

void workerpool_delete(struct workerpool *pool) // FIXME
{
  (void) pool;
}
