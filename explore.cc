// rodney -- fractal zoom explorer bot
// Copyright (C) 2020,2021 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

#include <cassert>

#include "rodney.h"

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#else
#include "worker.h"
#endif

#include <complex> // for complex sqrt, needed once

// distance estimate statistics

struct stats
{
  vec2 mean_small;
  vec2 mean_large;
  vec2 mean_csqr;
  mat2 cov_small;
  mat2 cov_large;
};

// distance estimate image (de normalized vs pixel spacing)

struct de_view
{
  int width;
  int height;
  int xstride;
  int ystride;
  float *de_x;
  float *de_y;
};

// compute distance estimate image statistics

static inline stats statistics(const de_view &image) noexcept
{
  const double squared_length_threshold = 4;
  stats s =
    { { { 0.0, 0.0 } }, { { 0.0, 0.0 } }, { { 0.0, 0.0 } }
    , { { { 0.0, 0.0 }, { 0.0, 0.0 } } }
    , { { { 0.0, 0.0 }, { 0.0, 0.0 } } } };
  double n_small = 0, n_large = 0;
  for (int j = 0; j < image.height; ++j)
  {
    for (int i = 0; i < image.width; ++i)
    {
      int ix = j * image.ystride + i * image.xstride;
      double x = image.de_x[ix];
      double y = image.de_y[ix];
      double squared_length = x * x + y * y;
      if (squared_length < squared_length_threshold)
      {
        if (0 < squared_length)
        {
          s.mean_small.v[0] += x;
          s.mean_small.v[1] += y;
          n_small += 1;
          // complex squaring of DE
          // makes limit distribution of hard-skewed locations unipolar
          double t = x * x - y * y;
          y = 2 * x * y;
          x = t;
          s.mean_csqr.v[0] += x;
          s.mean_csqr.v[1] += y;
        }
      }
      else
      {
        s.mean_large.v[0] += x;
        s.mean_large.v[1] += y;
        n_large += 1;
      }
    }
  }
  s.mean_small.v[0] /= n_small;
  s.mean_small.v[1] /= n_small;
  s.mean_large.v[0] /= n_large;
  s.mean_large.v[1] /= n_large;
  s.mean_csqr.v[0] /= n_small;
  s.mean_csqr.v[1] /= n_small;
  for (int j = 0; j < image.height; ++j)
  {
    for (int i = 0; i < image.width; ++i)
    {
      int ix = j * image.ystride + i * image.xstride;
      double x = image.de_x[ix];
      double y = image.de_y[ix];
      double squared_length = x * x + y * y;
      if (squared_length < squared_length_threshold)
      {
        if (0 < squared_length)
        {
          double dx = x - s.mean_small.v[0];
          double dy = y - s.mean_small.v[1];
          s.cov_small.m[0][0] += dx * dx;
          s.cov_small.m[0][1] += dx * dy;
          s.cov_small.m[1][1] += dy * dy;
        }
      }
      else
      {
        double dx = x - s.mean_large.v[0];
        double dy = y - s.mean_large.v[1];
        s.cov_large.m[0][0] += dx * dx;
        s.cov_large.m[0][1] += dx * dy;
        s.cov_large.m[1][1] += dy * dy;
      }
    }
  }
  s.cov_small.m[0][0] /= n_small;
  s.cov_small.m[0][1] /= n_small;
  s.cov_small.m[1][1] /= n_small;
  s.cov_small.m[1][0] = s.cov_small.m[0][1];
  s.cov_large.m[0][0] /= n_large;
  s.cov_large.m[0][1] /= n_large;
  s.cov_large.m[1][1] /= n_large;
  s.cov_large.m[1][0] = s.cov_large.m[0][1];
  return s;
}

// sub-quadrants

enum subquadrant
{
  PARENT = 0,
  BL = 1,
  BM = 2,
  BR = 3,
  ML = 4,
  MM = 5,
  MR = 6,
  TL = 7,
  TM = 8,
  TR = 9,
  SUBQUADRANTS = 10
};

// sub-quadrants of a view

struct quadrants_view
{
  de_view quadrant[SUBQUADRANTS];
};

// image quadrants (views reference parent's image data, no copying)

static inline quadrants_view view_quadrants(de_view &image) noexcept
{
  quadrants_view q;
  q.quadrant[PARENT] = image;
  int k = BL;
  for (int j = 0; j < 3; ++j)
  {
    for (int i = 0; i < 3; ++i)
    {
      int ix
        = j * (image.height / 4) * image.ystride
        + i * (image.width / 4) * image.xstride;
      q.quadrant[k].width = image.width / 2;
      q.quadrant[k].height = image.height / 2;
      q.quadrant[k].xstride = image.xstride;
      q.quadrant[k].ystride = image.ystride;
      q.quadrant[k].de_x = image.de_x + ix;
      q.quadrant[k].de_y = image.de_y + ix;
      ++k;
    }
  }
  return q;
}

// statistics of subquadrants

struct quadrants_stats
{
  stats quadrant[SUBQUADRANTS];
};

// compute statistics of quadrants

static inline quadrants_stats statistics_quadrants(const quadrants_view &q) noexcept
{
  // TODO optimize by combining statistics of indiviual 16ths
  quadrants_stats s;
  for (int k = PARENT; k < SUBQUADRANTS; ++k)
  {
    s.quadrant[k] = statistics(q.quadrant[k]);
  }
  return s;
}

// metrics (smaller is better)

struct de_metric
{
  double centrality;
  double isotropy;
  double spread;
};

// compute metrics from statistics

static inline de_metric metric(const stats &s) noexcept
{
  de_metric m;
  m.centrality = s.mean_small.v[0] * s.mean_small.v[0] + s.mean_small.v[1] * s.mean_small.v[1];
  m.isotropy = std::log(eigenvalue_ratio(s.cov_small));
  m.spread = 1.0 / det(s.cov_large);
  return m;
}

// compute score (smaller is better) from metric and weight

static inline double score(const de_metric &w, const de_metric &m) noexcept
{
  return
    w.centrality * m.centrality +
    w.isotropy * m.isotropy +
    w.spread * m.spread;
}

// allocate buffers

static inline de_view de_view_new(int w, int h) // throws
{
  // dimensions must be multiples of 4
  assert(! (w & 3));
  assert(! (h & 3));
  de_view de;
  de.width = w;
  de.height = h;
  de.xstride = 1;
  de.ystride = w;
  de.de_x = new float[w * h];
  de.de_y = new float[w * h];
  return de;
}

static inline void de_view_delete(de_view &de)
{
  delete[] de.de_x;
  delete[] de.de_y;
  de.de_x = nullptr;
  de.de_y = nullptr;
}

// zoom in to a quadrant

static inline coordinates zoom(const coordinates &z, subquadrant q, double aspect, const mat2 &skew) noexcept
{
  int k = BL;
  for (int j = -1; j <= 1; ++j)
  {
    for (int i = -1; i <= 1; ++i)
    {
      if (k == q)
      {
        vec2 u = { { i * z.r / 2 * aspect, j * z.r / 2 } };
        vec2 v = skew * u;
        coordinates zz =
          { z.cx + v.v[0]
          , z.cy + v.v[1]
          , z.r / 2
          };
        return zz;
      }
      ++k;
    }
  }
  // zoom out
  coordinates zz = { z.cx, z.cy, z.r * 2 };
  return zz;
}

// zoom in to most exterior pixel

static inline coordinates zoom_to_exterior(const coordinates &z, const mat2 &skew, const de_view &de) noexcept
{
  double maximum = 0;
  double maximum_cx = z.cx;
  double maximum_cy = z.cy;
  const int w = de.width;
  const int h = de.height;
  const double s = 2 * z.r / h;
  for (int j = 0; j < h; ++j)
  {
    for (int i = 0; i < w; ++i)
    {
      int ix = j * de.ystride + i * de.xstride;
      double dx = de.de_x[ix];
      double dy = de.de_y[ix];
      double d = dx * dx + dy * dy;
      if (maximum < d)
      {
        // TODO jitter
        vec2 u = { { (i - w / 2) * s, (j - h / 2) * s } };
        vec2 v = skew * u;
        maximum = d;
        maximum_cx = v.v[0] + z.cx;
        maximum_cy = v.v[1] + z.cy;
      }
    }
  }
  coordinates zz = { maximum_cx, maximum_cy, z.r / 2 };
  return zz;
}

// randomize formula

static inline formula random_formula(uint32_t seed) noexcept
{
  formula f;
  f.ngroups = 1 +
    ( int(std::pow(U(seed, 1, 1), 2) * (FORMULA_GROUPS_MAX - 1))
    % FORMULA_GROUPS_MAX );
  f.loop_start = int(U(seed, 1, 2) * f.ngroups) % f.ngroups;
  for (int g = 0; g < f.ngroups; ++g)
  {
    f.group[g].repeats = 1 + int(std::pow(U(seed, 2, g), 3) * 60);
    f.group[g].power = 2 + int(std::pow(U(seed, 3, g), 3) * 6);
    f.group[g].abs_x = U(seed, 4, g) < 0.5;
    f.group[g].abs_y = U(seed, 5, g) < 0.5;
    f.group[g].neg_x = U(seed, 6, g) < 0.5;
    f.group[g].neg_y = U(seed, 7, g) < 0.5;
  }
  return f;
}

static inline colour random_colour(uint32_t seed, double sat_range, double val_range, double val_offset) noexcept
{
  colour c;
  double r = sat_range * U(seed, 1, 1);
  double t = 2 * 3.141592653589793 * U(seed, 1, 2);
  c.v.v[0] = r * std::cos(t);
  c.v.v[1] = r * std::sin(t);
  c.v.v[2] = val_range * U(seed, 1, 3) - val_offset;
  return c;
}

static inline multiwave random_multiwave(uint32_t seed) noexcept
{
  multiwave mw;
  mw.nwaves = MAX_WAVES;
  double period = 1;
  for (int w = 0; w < mw.nwaves; ++w)
  {
    mw.blend[w] = 0.5 * U(seed, w * MAX_COLOURS, 6);
    mw.waves[w].ncolours = 3 + U(seed, w * MAX_COLOURS, 0) * (MAX_COLOURS - 3);
    mw.waves[w].offset = U(seed, w * MAX_COLOURS, 1) * mw.waves[w].ncolours;
    mw.waves[w].period = period;
    period *= (2 + 6 * U(seed, w * MAX_COLOURS, 2));
    for (int k = 0; k < mw.waves[w].ncolours; ++k)
    {
      mw.waves[w].colours[k] = random_colour(burtle_hash(seed ^ (w * MAX_COLOURS + k)), 16, 16, 8);
    }
  }
  const int maxiters = 65536;
  const int aa = 8;
  linearRGB l = { { { 0, 0, 0 } } };
  for (int n = 0; n < maxiters * aa; ++n)
  {
    l.v = l.v + to_linearRGB(multiwave_colour(mw, n / (double) aa)).v;
  }
  l.v = l.v * float(2.0 / (maxiters * aa));
  mw.grey_point = l;
  return mw;
}

static inline palette random_palette(uint32_t seed)
{
  palette p;
  p.n = random_multiwave(burtle_hash(seed ^ 3951282789u));
  p.s = random_multiwave(burtle_hash(seed ^ 1784947738u));
  p.blend_ns = U(seed, 1, 1);
  float t = 2 * 3.141592653589793 * U(seed, 1, 2);
  p.slope_x = std::cos(t);
  p.slope_y = std::sin(t);
  p.slope_power = U(seed, 1, 3) * 10;
  p.slope_light = random_colour(burtle_hash(seed ^ 1741549489u), 4, 8, 0);
  p.slope_dark  = random_colour(burtle_hash(seed ^ 3137718159u), 4, 8, -8);
  p.slope_grey.v = (p.slope_light.v + p.slope_dark.v) * 0.5;
  p.blend_slope = U(seed, 1, 5);
  p.stripe_power = 1 + 8 * U(seed, 1, 6);
  p.stripe_decay = std::exp2(-0.25 * U(seed, 1, 7));
  return p;
}

static inline int render_image(de_view &img, const coordinates &co, mat2 &skew, const formula &f, const int &maxiters, int64_t &cost, const bool all_interior) noexcept
{
  const int h = img.height;
  const int w = img.width;
  const double s = 2 * co.r / h;
  int total_escaped = 0;
  int total_escaped_late = 0;
  for (int j = 0; j < h; ++j)
  {
    for (int i = 0; i < w; ++i)
    {
      float dex = 0.0f;
      float dey = 0.0f;
      if (! all_interior)
      {
        // TODO jitter
        vec2 u = { { (i - w / 2) * s, (j - h / 2) * s } };
        vec2 v = skew * u;
        complex c =
          { dual(v.v[0] + co.cx, 0)
          , dual(v.v[1] + co.cy, 1)
          };
        complex z = c;
        int stanza = 0;
        int count = 0;
        for (int k = 0; k < maxiters; ++k)
        {
          if (norm(z) > 65536.0)
          {
            const vec2 u = { { z.re.x, z.im.x } };
            const mat2 J =
              { { { z.re.dx[0] * s, z.re.dx[1] * s }
                , { z.im.dx[0] * s, z.im.dx[1] * s } } };
            const double num = length(u) * log(length(u));
            const vec2 den = normalize(u) * J * skew;
            const double m = num / norm(den);
            dex =  m * den.v[0];
            dey = -m * den.v[1];
            if (! std::isfinite(dex) || ! std::isfinite(dey))
            {
              dex = 0.0f;
              dey = 0.0f;
            }
            total_escaped++;
            if (k >= maxiters / 2)
            {
              total_escaped_late++;
            }
            break;
          }
          // step formula
          if (++count >= f.group[stanza].repeats)
          {
            count = 0;
            if (++stanza >= f.ngroups)
            {
              stanza = f.loop_start;
            }
          }
          int power = f.group[stanza].power;
          if (f.group[stanza].abs_x) z.re = abs(z.re);
          if (f.group[stanza].abs_y) z.im = abs(z.im);
          if (f.group[stanza].neg_x) z.re = -z.re;
          if (f.group[stanza].neg_y) z.im = -z.im;
          z = pow(z, power) + c;
          ++cost;
        }
      }
      int ix = j * img.ystride + i * img.xstride;
      img.de_x[ix] = dex;
      img.de_y[ix] = dey;
    }
  }
  if (total_escaped_late > 0.1 * total_escaped && maxiters <= 0x10000000)
  {
    return maxiters * 2;
  }
  else
  {
    return maxiters;
  }
}

static inline int find_period(const coordinates &co, const formula &f, const mat2 &skew, const int &maxiters, int64_t &cost) noexcept
{
  const double A = co.cx;
  const double B = co.cy;
  const double S = co.r;
  const mat2 K = skew;
  dual cx(A, 0);
  dual cy(B, 1);
  complex c = { cx, cy };
  complex z = { 0, 0 };
  // K^{-1}
  double ai = K.m[0][0];
  double aj = K.m[0][1];
  double bi = K.m[1][0];
  double bj = K.m[1][1];
  double detK = ai * bj - aj * bi;
  double ai1 =  bj / detK;
  double aj1 = -bi / detK;
  double bi1 = -aj / detK;
  double bj1 =  ai / detK;
  double z2 = 0;
  double r2 = 1e50;
  bool p = true;
  int i = 0;
  int count = 0;
  int stanza = 0;
  while (i < maxiters && z2 < r2 && p)
  {
    // step formula
    if (++count >= f.group[stanza].repeats)
    {
      count = 0;
      if (++stanza >= f.ngroups)
      {
        stanza = f.loop_start;
      }
    }
    int power = f.group[stanza].power;
    if (f.group[stanza].abs_x) z.re = abs(z.re);
    if (f.group[stanza].abs_y) z.im = abs(z.im);
    if (f.group[stanza].neg_x) z.re = -z.re;
    if (f.group[stanza].neg_y) z.im = -z.im;
    z = pow(z, power) + c;
    ++cost;
    const double x = z.re.x;
    const double y = z.im.x;
    const double xa = z.re.dx[0];
    const double xb = z.re.dx[1];
    const double ya = z.im.dx[0];
    const double yb = z.im.dx[1];
    // J^{-1}
    const double detJ = xa * yb - xb * ya;
    const double xa1 =  yb / detJ;
    const double xb1 = -ya / detJ;
    const double ya1 = -xb / detJ;
    const double yb1 =  xa / detJ;
    // (u0 v0) = J^{-1} * (x y)
    const double u0 = xa1 * x + xb1 * y;
    const double v0 = ya1 * x + yb1 * y;
    // (u1 v1) = s^{-1} K^{-1} * (u0 v0)
    const double u1 = (ai1 * u0 + aj1 * v0) / S;
    const double v1 = (bi1 * u0 + bj1 * v0) / S;
    double u2 = double(u1);
    double v2 = double(v1);
    double uv = u2 * u2 + v2 * v2;
    p = 1 <= uv;
    ++i;
    double xf = double(x);
    double yf = double(y);
    z2 = xf * xf + yf * yf;
  }
  if (i == maxiters || r2 <= z2 || p)
  {
    i = -1;
  }
  return i;
}

static inline vec2 find_zero(const vec2 &guess, const formula &f, const int &period, int64_t &cost) noexcept
{
  double x = guess.v[0];
  double y = guess.v[1];
  for (int n = 0; n < 24; ++n)
  {
    complex c =
      { dual(x, 0)
      , dual(y, 1)
      };
    complex z = c;
    int stanza = 0;
    int count = 0;
    for (int k = 1; k < period; ++k)
    {
      // step formula
      if (++count >= f.group[stanza].repeats)
      {
        count = 0;
        if (++stanza >= f.ngroups)
        {
          stanza = f.loop_start;
        }
      }
      int power = f.group[stanza].power;
      if (f.group[stanza].abs_x) z.re = abs(z.re);
      if (f.group[stanza].abs_y) z.im = abs(z.im);
      if (f.group[stanza].neg_x) z.re = -z.re;
      if (f.group[stanza].neg_y) z.im = -z.im;
      z = pow(z, power) + c;
      ++cost;
    }
    const vec2 f = { { z.re.x, z.im.x } };
    const mat2 J =
      { { { z.re.dx[0], z.re.dx[1] }
        , { z.im.dx[0], z.im.dx[1] } } };
    vec2 v = inv(J) * f;
    x -= v.v[0];
    y -= v.v[1];
    if (! (v.v[0] * v.v[0] + v.v[1] * v.v[1] > 1e-16))
    {
      break;
    }
  }
  const vec2 u = { { x, y } };
  return u;
}

static inline double find_size(const vec2 &c0, const formula &f, const int &period, int64_t &cost, mat2 &skew) noexcept
{
  // compute average degree
  double degree = 0;
  double ndegree = 0;
  // FIXME check if this init and formula below is equivalent to the et version...
  dual x(c0.v[0]); x.dx[0] = 1;
  dual y(c0.v[1]); y.dx[1] = 1;
  complex c = { x, y };
  complex z = c;
  double bxa = 1;
  double bxb = 0;
  double bya = 0;
  double byb = 1;
  int j = 1;
  int count = 0;
  int stanza = 0;
  while (j < period)
  {
    // step formula
    if (++count >= f.group[stanza].repeats)
    {
      count = 0;
      if (++stanza >= f.ngroups)
      {
        stanza = f.loop_start;
      }
    }
    int power = f.group[stanza].power;
    if (f.group[stanza].abs_x) z.re = abs(z.re);
    if (f.group[stanza].abs_y) z.im = abs(z.im);
    if (f.group[stanza].neg_x) z.re = -z.re;
    if (f.group[stanza].neg_y) z.im = -z.im;
    z = pow(z, power) + c;
    ++cost;
    // the degree of each stanza is the *lowest* non-linear power
    degree += std::log(double(f.group[stanza].power));
    ndegree += 1;
    const double lxa = z.re.dx[0];
    const double lxb = z.re.dx[1];
    const double lya = z.im.dx[0];
    const double lyb = z.im.dx[1];
    // step b
    const double det = lxa * lyb - lxb * lya;
    bxa = bxa + lyb / det;
    bxb = bxb - lxb / det;
    bya = bya - lya / det;
    byb = byb + lxa / det;
    ++j;
  }
  degree = std::exp(degree / ndegree);
  double deg = degree / (degree - 1);
  if (std::isnan(deg) || std::isinf(deg)) deg = 0;
  // l^d b
  const double lxa = z.re.dx[0];
  const double lxb = z.re.dx[1];
  const double lya = z.im.dx[0];
  const double lyb = z.im.dx[1];
  const double l = std::sqrt(std::abs(lxa * lyb - lxb * lya));
  const double beta = std::sqrt(std::abs(bxa * byb - bxb * bya));
  const double llb = std::exp(std::log(l) * deg) * beta;
  const double S = 1 / llb;
  byb =   byb / beta;
  bxb = - bxb / beta;
  bya = - bya / beta;
  bxa =   bxa / beta;
  const mat2 B = { { { byb, bxb }, { bya, byb } } };
  skew = B;
  return S;
}

static inline double find_de(const vec2 &c0, const formula &f, const int &maxiters, int64_t &cost) noexcept
{
  complex c =
    { dual(c0.v[0], 0)
    , dual(c0.v[1], 1)
    };
  complex z = c;
  int stanza = 0;
  int count = 0;
  for (int k = 0; k < maxiters; ++k)
  {
    if (norm(z) > 65536.0)
    {
      return 1;
    }
    // step formula
    if (++count >= f.group[stanza].repeats)
    {
      count = 0;
      if (++stanza >= f.ngroups)
      {
        stanza = f.loop_start;
      }
    }
    int power = f.group[stanza].power;
    if (f.group[stanza].abs_x) z.re = abs(z.re);
    if (f.group[stanza].abs_y) z.im = abs(z.im);
    if (f.group[stanza].neg_x) z.re = -z.re;
    if (f.group[stanza].neg_y) z.im = -z.im;
    z = pow(z, power) + c;
    ++cost;
  }
  return 0;
}

struct context
{
  de_metric weights;
  formula f;
  double aspect;
  int maxiters[64];
  mat2 skew;
  palette pal;
  int current;
  int next;
  coordinates coords[2];
  de_view render[2];
  quadrants_view qview[2];
  int64_t cost;
  int endgame; // > 0 -> exterior ; < 0 -> interior
  bool verbose;
  uint32_t seed;
};

static inline void context_reset(context &ctx, uint32_t seed) noexcept
{
  ctx.seed = seed;
#ifdef RODNEY_VERBOSE
  std::fprintf(stdout, "using seed %u\n", seed);
#endif
  // unzoomed coordinates / view
  const coordinates unzoomed = { 4 * U(seed, 1, 1) - 2, 4 * U(seed, 1, 2) - 2, 4.0 };
  ctx.weights.centrality = U(seed, 1, 3);
  ctx.weights.isotropy = U(seed, 1, 4);
  ctx.weights.spread = U(seed, 1, 5);
#ifdef RODNEY_VERBOSE
  std::fprintf
    ( stdout
    , "generated weights (%g, %g, %g)\n"
    , ctx.weights.centrality
    , ctx.weights.isotropy
    , ctx.weights.spread
    );
#endif
  ctx.f = random_formula(burtle_hash(seed ^ 3638938372u));
#ifdef RODNEY_VERBOSE
  std::fprintf
    ( stdout
    , "generated formula with %d groups with loop starting at %d:\n"
    , ctx.f.ngroups
    , ctx.f.loop_start
    );
  for (int g = 0; g < ctx.f.ngroups; ++g)
  {
    std::fprintf
      ( stdout
      , "  group %d: %d %d %d %d %d %d\n"
      , g
      , ctx.f.group[g].repeats, ctx.f.group[g].power
      , ctx.f.group[g].abs_x, ctx.f.group[g].abs_y
      , ctx.f.group[g].neg_x, ctx.f.group[g].neg_y
      );
  }
#endif
  ctx.pal = random_palette(burtle_hash(seed ^ 2771558299u));
  ctx.maxiters[0] = 64;
  ctx.current = 0;
  ctx.next = 1;
  ctx.coords[0] = unzoomed;
  ctx.coords[1] = unzoomed;
  ctx.cost = 0;
  ctx.skew = { { { 1.0, 0.0 }, { 0.0, 1.0 } } };
  ctx.endgame = 0;
}

static inline context context_new(int width, int height) // throws
{
  context ctx;
  ctx.render[0] = de_view_new(width, height);
  ctx.render[1] = de_view_new(width, height);
  ctx.qview[0] = view_quadrants(ctx.render[0]);
  ctx.qview[1] = view_quadrants(ctx.render[1]);
  ctx.aspect = width / (double) height;
  return ctx;
}

static inline void context_delete(context &ctx)
{
  de_view_delete(ctx.render[0]);
  de_view_delete(ctx.render[1]);
}

static inline coordinates context_finish(context &ctx, int n) noexcept
{
  // FIXME ensure uniform boundary
  for (int k = n + 1; k < 64; ++k)
  {
    ctx.maxiters[k] = ctx.maxiters[n];
  }
  return ctx.coords[ctx.current];
}

static inline void context_step(context &ctx, const int n) noexcept
{
  bool all_interior = n != 0;
  for (int j = 0; j < ctx.render[ctx.current].height && all_interior; ++j)
  {
    for (int i = 0; i < ctx.render[ctx.current].width && all_interior; ++i)
    {
      int ix = j * ctx.render[ctx.current].ystride + i * ctx.render[ctx.current].xstride;
      float dex = ctx.render[ctx.current].de_x[ix];
      float dey = ctx.render[ctx.current].de_y[ix];
      if (dex * dex + dey * dey > 0)
      {
        all_interior = false;
      }
    }
  }
  { int tmp = ctx.current; ctx.current = ctx.next; ctx.next = tmp; }
  ctx.maxiters[n+1] = render_image
    ( ctx.render[ctx.current]
    , ctx.coords[ctx.current]
    , ctx.skew
    , ctx.f
    , ctx.maxiters[n]
    , ctx.cost
    , all_interior
    );
  if (ctx.endgame > 0)
  {
    ctx.coords[ctx.next] = zoom_to_exterior(ctx.coords[ctx.current], ctx.skew, ctx.render[ctx.current]);
    if (ctx.verbose)
    {
      std::fprintf
        ( stdout
        , "%u zooming to exterior at radius %g\n"
        , ctx.seed
        , ctx.coords[ctx.current].r
        );
    }
  }
  else if (ctx.endgame < 0)
  {
    ctx.coords[ctx.next] = ctx.coords[ctx.current];
    ctx.coords[ctx.next].r /= 2;
    if (ctx.verbose)
    {
      std::fprintf
        ( stdout
        , "%u zooming to interior at radius %g\n"
        , ctx.seed
        , ctx.coords[ctx.current].r
        );
    }
  }
  else
  {
    quadrants_stats s = statistics_quadrants(ctx.qview[ctx.current]);
    double minimum_score = 1.0 / 0.0;
    int minimum_quadrant = MM;
    for (int q = BL; q < SUBQUADRANTS; ++q)
    {
      double points = score(ctx.weights, metric(s.quadrant[q]));
      if (minimum_score > points)
      {
        minimum_score = points;
        minimum_quadrant = q;
      }
    }
    ctx.coords[ctx.next] = zoom
      ( ctx.coords[ctx.current]
      , subquadrant(minimum_quadrant)
      , ctx.aspect
      , ctx.skew
      );
    // get mean of complex de^2
    double x = s.quadrant[minimum_quadrant].mean_csqr.v[0];
    double y = s.quadrant[minimum_quadrant].mean_csqr.v[1];
    // the branch cut in square root doesn't matter
    // stretching up is the same as stretching down
    std::complex<double> dir = std::sqrt(std::complex<double>(x, y));
    // normalize
    x = std::real(dir);
    y = std::imag(dir);
    double d = 1 / std::sqrt(x * x + y * y);
    x *= d;
    y *= d;
    // formulate skew matrix
    double stretch = std::sqrt(eigenvalue_ratio(s.quadrant[minimum_quadrant].cov_small));
    mat2 P = { { { x, -y }, { y, x } } };
    mat2 P1 = { { { x, y }, { -y, x } } };
    mat2 S = { { { 1 / stretch, 0 }, { 0, stretch } } };
    // no need for Jacobians, because it is a directional DE to a shape
    mat2 skew_delta = P1 * S * P;
    mat2 skew_next = ctx.skew * skew_delta;
    d = det(skew_next);
    if (! (std::isnan(d) || std::isinf(d)))
    {
      ctx.skew = skew_next;
    }
    if (ctx.verbose)
    {
      std::fprintf
        ( stdout
        , "%u zooming to quadrant %d at radius %g with score %g stretch %g\n"
        , ctx.seed
        , minimum_quadrant
        , ctx.coords[ctx.current].r
        , minimum_score
        , stretch
        );
    }
  }
}

extern explore_output explore1(const explore_input &in)
{
  const int64_t maximum_cost = 1LL << 27LL;
  uint32_t seed = in.seed;
  context ctx = context_new(1 << in.quality, 1 << in.quality);
  ctx.verbose = in.verbose;
#if 0
  uint32_t j = 0;
#endif
  explore_output out;
  context_reset(ctx, seed);
  int k = 0;
  int end_k = 64;
  for (; k < 36 ;)
  {
    context_step(ctx, k++);
    if (ctx.cost >= maximum_cost)
    {
      break;
    }
#if 0
    if (24 < k)
    {
      int period = find_period(ctx.coords[ctx.current], ctx.f, ctx.skew, ctx.maxiters[k], ctx.cost);
      if (ctx.verbose)
      {
        std::fprintf(stdout, "%u.%d found period %d\n", seed, j, period);
      }
      if (period > 0)
      {
        vec2 guess = { { ctx.coords[ctx.current].cx, ctx.coords[ctx.current].cy } };
        vec2 c0 = find_zero(guess, ctx.f, period, ctx.cost);
        if (ctx.verbose)
        {
          std::fprintf(stdout, "%u.%d found zero %g + i %g\n", seed, j, c0.v[0], c0.v[1]);
        }
        mat2 skew = { { { 1, 0 }, { 0, 1 } } };
        double S = find_size(c0, ctx.f, period, ctx.cost, skew);
        if (ctx.verbose)
        {
          std::fprintf(stdout, "%u.%d found size %g skew %g %g %g %g\n", seed, j, S, skew.m[0][0], skew.m[0][1], skew.m[1][0], skew.m[1][1]);
        }
        if (1e-4 > S && S > 1e-14)
        {
          if (find_de(c0, ctx.f, ctx.maxiters[k], ctx.cost) <= 0)
          {
            ctx.coords[ctx.current].cx = ctx.coords[ctx.next].cx = c0.v[0];
            ctx.coords[ctx.current].cy = ctx.coords[ctx.next].cy = c0.v[1];
            ctx.coords[ctx.next].r = ctx.coords[ctx.current].r / 2;
            ctx.skew = skew;
            ctx.endgame = -1;
            break;
          }
        }
      }
    }
#endif
  }
  if (ctx.endgame == 0)
  {
    ctx.endgame = 1;
    end_k = k + 16;
  }
  for (; k < end_k ;)
  { 
    context_step(ctx, k++);
  }
  coordinates co = context_finish(ctx, k);
  out.f = ctx.f;
  for (int i = 0; i < 64; ++i)
  {
    out.maxiters[i] = ctx.maxiters[i];
  }
  out.end = co;
  out.skew = ctx.skew;
  out.pal = ctx.pal;
  out.pal.parity = 1;
  out.seed = seed;
  context_delete(ctx);
  return out;
}

// worker entry point

#ifdef __EMSCRIPTEN__

#ifdef RODNEY_WORKER

extern "C" void explore(char *data, int size)
{
  assert(size == sizeof(explore_input));
  explore_input *in = (explore_input *) data;
  try
  {
    explore_output out = explore1(*in);
    emscripten_worker_respond((char *) &out, sizeof(out));
  }
  catch (const std::exception &e)
  {
    emscripten_worker_respond(0, 0);
  }
}

#endif

#else

struct work explore(struct work job)
{
  assert(job.size == sizeof(explore_input));
  explore_input *in = (explore_input *) job.data;
  try
  {
    explore_output out = explore1(*in);
    struct work result;
    result.size = sizeof(explore_output);
    result.data = (char *) std::malloc(result.size);
    std::memcpy(result.data, &out, result.size);
    return result;
  }
  catch (const std::exception &e)
  {
    struct work result = { 0, 0 };
    return result;
  }
}

#endif
