// rodney -- fractal zoom explorer bot
// Copyright (C) 2020 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

struct work
{
  char *data;
  int size;
};

struct workerpool;

#ifdef __EMSCRIPTEN__
struct workerpool *workerpool_new(int threads, const char *url, const char *name);
#else
typedef struct work (*worker)(struct work);
struct workerpool *workerpool_new(int threads, worker callback);
#endif
void workerpool_delete(struct workerpool *pool);
void workerpool_put(struct workerpool *pool, struct work job);
bool workerpool_empty(struct workerpool *pool);
struct work workerpool_get(struct workerpool *pool);
