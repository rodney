// rodney -- fractal zoom explorer bot
// Copyright (C) 2020,2021 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

#pragma once

#include <cassert>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <algorithm>
#include <complex>

// pseudo random number generation
// <http://www.burtleburtle.net/bob/hash/integer.html>

static inline uint32_t burtle_hash(uint32_t a) noexcept
{
  a = (a+0x7ed55d16) + (a<<12);
  a = (a^0xc761c23c) ^ (a>>19);
  a = (a+0x165667b1) + (a<<5);
  a = (a+0xd3a2646c) ^ (a<<9);
  a = (a+0xfd7046c5) + (a<<3);
  a = (a^0xb55a4f09) ^ (a>>16);
  return a;
}

// uniform in [0,1)

static inline double U(uint32_t x, uint32_t y, uint32_t c) noexcept
{
  double msb = burtle_hash(x + burtle_hash(y + burtle_hash(2 * c + 0)))
             / (double) (0x100000000LL);
  double lsb = burtle_hash(x + burtle_hash(y + burtle_hash(2 * c + 1)))
             / (double) (0x100000000LL) / (double) (0x100000000LL);
  return msb + lsb;
}

// squaring

static inline double sqr(double x) noexcept
{
  return x * x;
}

// 2 vector

struct vec2
{
  double v[2];
};

static inline double norm(const vec2 &v) noexcept
{
  return v.v[0] * v.v[0] + v.v[1] * v.v[1];
}

static inline double length(const vec2 &v) noexcept
{
  return std::sqrt(norm(v));
}

static inline vec2 normalize(const vec2 &v) noexcept
{
  double l = length(v);
  if (l > 0.0)
  {
    vec2 r = { { v.v[0] / l, v.v[1] / l } };
    return r;
  }
  return v; // zero or NaN
}

static inline vec2 operator*(const vec2 &v, const double &m) noexcept
{
  vec2 r = { { v.v[0] * m, v.v[1] * m } };
  return r;
}

// 2x2 matrix

struct mat2
{
  double m[2][2];
};

static inline bool isfinite(const mat2 &m) noexcept
{
  return
    std::isfinite(m.m[0][0]) &&
    std::isfinite(m.m[0][1]) &&
    std::isfinite(m.m[1][0]) &&
    std::isfinite(m.m[1][1]);
}

static inline mat2 transpose(const mat2 &m) noexcept
{
  mat2 r = { { { m.m[0][0], m.m[1][0] }, { m.m[0][1], m.m[1][1] } } };
  return r;
}

static inline vec2 operator*(const vec2 &v, const mat2 &m) noexcept
{
  vec2 r =
    { { v.v[0] * m.m[0][0] + v.v[1] * m.m[1][0]
      , v.v[0] * m.m[0][1] + v.v[1] * m.m[1][1] } };
  return r;
}

static inline mat2 operator*(const mat2 &v, const mat2 &m) noexcept
{
  mat2 r =
    { { { v.m[0][0] * m.m[0][0] + v.m[0][1] * m.m[1][0]
        , v.m[0][0] * m.m[0][1] + v.m[0][1] * m.m[1][1] }
      , { v.m[1][0] * m.m[0][0] + v.m[1][1] * m.m[1][0]
        , v.m[1][0] * m.m[0][1] + v.m[1][1] * m.m[1][1] } } };
  return r;
}

static inline vec2 operator*(const mat2 &v, const vec2 &m) noexcept
{
  vec2 r =
    { { v.m[0][0] * m.v[0] + v.m[0][1] * m.v[1]
      , v.m[1][0] * m.v[0] + v.m[1][1] * m.v[1] } };
  return r;
}

static inline mat2 operator/(const mat2 &m, const double &x) noexcept
{
  mat2 r =
    { { { m.m[0][0] / x, m.m[0][1] / x }
      , { m.m[1][0] / x, m.m[1][1] / x } } };
  return r;
}

// determinant of a matrix

static inline double det(const mat2 &m) noexcept
{
  return m.m[0][0] * m.m[1][1] - m.m[0][1] * m.m[1][0];
}

// inverse of a matrix

static inline mat2 inv(const mat2 &m) noexcept
{
  mat2 r = { { { m.m[0][0], -m.m[1][0] }, { -m.m[0][1], m.m[1][1] } } };
  return r / det(m);
}

// compute eigenvalues
// <http://people.math.harvard.edu/~knill/teaching/math21b2004/exhibits/2dmatrices/index.html>

static inline vec2 eigenvalues(const mat2 &m) noexcept
{
  const double t = (m.m[0][0] + m.m[1][1]) / 2.0;
  const double d = std::sqrt(std::max(0.0, t * t - det(m)));
  const double e_add = t + d;
  const double e_sub = t - d;
  vec2 r = { { e_add, e_sub } };
  return r;
}

// ratio of eigenvalues of a 2x2 covariance matrix (largest / smallest)

static inline double eigenvalue_ratio(const mat2 &m) noexcept
{
  vec2 e = eigenvalues(m);
  const double e_max = std::max(std::abs(e.v[0]), std::abs(e.v[1]));
  const double e_min = std::min(std::abs(e.v[0]), std::abs(e.v[1]));
  return e_max / e_min;
}

// image

struct image
{
  int width;
  int height;
  int cstride;
  int xstride;
  int ystride;
  int bytes;
  unsigned char *rgb;
};

// create image

static inline image image_new(int w, int h) // throws
{
  // dimensions must be multiples of 4
  assert(! (w & 3));
  assert(! (h & 3));
  image i;
  i.width = w;
  i.height = h;
  i.cstride = 1;
  i.xstride = 3;
  i.ystride = 3 * w;
  i.bytes = w * h * 3;
  i.rgb = new unsigned char[i.bytes];
  return i;
}

static inline void image_delete(image &i)
{
  delete[] i.rgb;
  i.rgb = nullptr;
}

// coordinates

struct coordinates
{
  // TODO support higher precision?
  double cx;
  double cy;
  double r;
};

// dual numbers for automatic differentiation

struct dual
{
  double x;
  double dx[2];

	dual() = default; // POD
	dual(const dual &a) = default; // POD
	dual(dual &&a) = default; // POD
	dual &operator=(const dual &a) = default; // POD
	dual &operator=(dual &&a) = default; // POD
	~dual() = default; // POD;

  inline dual(const double &a) noexcept
  : x(a)
  {
    for (int d = 0; d < 2; ++d)
    {
      dx[d] = 0;
    }
  }

  inline dual(const double &a, int i) noexcept
  : x(a)
  {
    for (int d = 0; d < 2; ++d)
    {
      dx[d] = double(d == i);
    }
  }

};

static inline dual operator+(const dual &a, const dual &b) noexcept
{
  dual r;
  r.x = a.x + b.x;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d] + b.dx[d];
  }
  return r;
}

static inline dual operator+(const dual &a, const double &b) noexcept
{
  dual r;
  r.x = a.x + b;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d];
  }
  return r;
}

static inline dual operator+(const double &a, const dual &b) noexcept
{
  dual r;
  r.x = a + b.x;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = b.dx[d];
  }
  return r;
}

static inline dual operator-(const double &a, const dual &b) noexcept
{
  dual r;
  r.x = a - b.x;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = -b.dx[d];
  }
  return r;
}

static inline dual operator-(const dual &a, const double &b) noexcept
{
  dual r;
  r.x = a.x - b;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d];
  }
  return r;
}

static inline dual operator-(const dual &a, const dual &b) noexcept
{
  dual r;
  r.x = a.x - b.x;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d] - b.dx[d];
  }
  return r;
}

static inline dual operator-(const dual &b) noexcept
{
  dual r;
  r.x = - b.x;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = - b.dx[d];
  }
  return r;
}

static inline dual operator*(const dual &a, const dual &b) noexcept
{
  dual r;
  r.x = a.x * b.x;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d] * b.x + a.x * b.dx[d];
  }
  return r;
}

static inline dual sqr(const dual &a) noexcept
{
  dual r;
  r.x = sqr(a.x);
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = 2 * a.dx[d] * a.x;
  }
  return r;
}

static inline dual operator/(const dual &a, const dual &b) noexcept
{
  dual r;
  r.x = a.x * b.x;
  double den = 1 / (b.x * b.x);
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = (a.dx[d] * b.x - a.x * b.dx[d]) * den;
  }
  return r;
}

static inline dual operator*(const double &a, const dual &b) noexcept
{
  dual r;
  r.x = a * b.x;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a * b.dx[d];
  }
  return r;
}

static inline dual operator*(const dual &a, const double &b) noexcept
{
  dual r;
  r.x = a.x * b;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d] * b;
  }
  return r;
}

static inline dual operator/(const dual &a, const double &b) noexcept
{
  dual r;
  r.x = a.x / b;
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d] / b;
  }
  return r;
}

static inline bool operator<(const dual &a, const double &b) noexcept
{
  return a.x < b;
}

static inline bool operator>(const dual &a, const double &b) noexcept
{
  return a.x > b;
}

static inline bool operator<=(const dual &a, const double &b) noexcept
{
  return a.x <= b;
}

static inline bool operator>=(const dual &a, const double &b) noexcept
{
  return a.x >= b;
}

static inline bool operator<=(const dual &a, const dual &b) noexcept
{
  return a.x <= b.x;
}

static inline bool operator>=(const dual &a, const dual &b) noexcept
{
  return a.x >= b.x;
}

static inline bool operator<(const dual &a, const dual &b) noexcept
{
  return a.x < b.x;
}

static inline bool operator>(const dual &a, const dual &b) noexcept
{
  return a.x > b.x;
}

static inline dual abs(const dual &a) noexcept
{
  return a.x < 0 ? -a : a;
}

static inline dual exp(const dual &a) noexcept
{
  using std::exp;
  dual r;
  r.x = exp(a.x);
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d] * r.x;
  }
  return r;
}

static inline dual cos(const dual &a) noexcept
{
  using std::cos;
  using std::sin;
  dual r;
  r.x = cos(a.x);
  const double s = -sin(a.x);
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d] * s;
  }
  return r;
}

static inline dual sin(const dual &a) noexcept
{
  using std::cos;
  using std::sin;
  dual r;
  r.x = sin(a.x);
  const double c = cos(a.x);
  for (int d = 0; d < 2; ++d)
  {
    r.dx[d] = a.dx[d] * c;
  }
  return r;
}

static inline dual &operator-=(dual &me, const dual &you) noexcept
{
  return me = me - you;
}

static inline dual &operator+=(dual &me, const dual &you) noexcept
{
  return me = me + you;
}

static inline dual diffabs(const double &c, const dual &d) noexcept
{
  const double cd = c + d.x;
  const dual c2d = 2 * c + d;
  return c >= 0.0 ? cd >= 0.0 ? d : -c2d : cd > 0.0 ? c2d : -d;
}

static inline dual diffabs(const dual &c, const dual &d) noexcept
{
  const double cd = c.x + d.x;
  const dual c2d = 2 * c + d;
  return c >= 0.0 ? cd >= 0.0 ? d : -c2d : cd > 0.0 ? c2d : -d;
}

// complex numbers

struct complex
{
  dual re;
  dual im;
};

static inline double norm(const complex &a) noexcept
{
  return a.re.x * a.re.x + a.im.x * a.im.x;
}

static inline double abs(const complex &a) noexcept
{
  return std::sqrt(norm(a));
}

static inline complex conj(const complex &a) noexcept
{
  complex r = { a.re, -a.im };
  return r;
}

static inline complex operator-(const complex &a) noexcept
{
  complex dc = { -a.re, -a.im };
  return dc;
}

static inline complex operator+(const complex &a, const complex &b) noexcept
{
  complex dc = { a.re + b.re, a.im + b.im };
  return dc;
}

static inline complex operator-(const complex &a, const complex &b) noexcept
{
  complex dc = { a.re - b.re, a.im - b.im };
  return dc;
}

static inline complex operator*(const double &a, const complex &b) noexcept
{
  complex dc = { a * b.re, a * b.im };
  return dc;
}

static inline complex operator*(const complex &a, const double &b) noexcept
{
  complex dc = { a.re * b, a.im * b };
  return dc;
}

static inline complex operator/(const complex &a, const double &b) noexcept
{
  complex dc = { a.re / b, a.im / b };
  return dc;
}

static inline complex operator*(const complex &a, const complex &b) noexcept
{
  complex dc = { a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re };
  return dc;
}

static inline complex sqr(const complex &a) noexcept
{
  complex dc = { sqr(a.re) - sqr(a.im), 2.0 * a.re * a.im };
  return dc;
}

static inline complex pow(complex x, int n) noexcept
{
  const complex one = { dual(1.0), dual(0.0) };
  switch (n)
  {
    case 0: return one;
    case 1: return x;
    case 2: return sqr(x);
    case 3: return x * sqr(x);
    case 4: return sqr(sqr(x));
    case 5: return x * sqr(sqr(x));
    case 6: return sqr(x * sqr(x));
    case 7: return x * sqr(x * sqr(x));
    case 8: return sqr(sqr(sqr(x)));
    default:
    {
      complex y = one;
      while (n > 1)
      {
        if (n & 1)
          y = y * x;
        x = sqr(x);
        n >>= 1;
      }
      return x * y;
    }
  }
}

// colouring algorithms

// 3 vector

struct vec3
{
  float v[3];
};

static inline float norm(const vec3 &v) noexcept
{
  return v.v[0] * v.v[0] + v.v[1] * v.v[1] + v.v[2] * v.v[2];
}

static inline float length(const vec3 &v) noexcept
{
  return std::sqrt(norm(v));
}

static inline vec3 normalize(const vec3 &v) noexcept
{
  float l = length(v);
  if (l > 0.0)
  {
    vec3 r = { { v.v[0] / l, v.v[1] / l, v.v[2] / l } };
    return r;
  }
  return v; // zero or NaN
}

static inline vec3 operator*(const vec3 &v, const float &m) noexcept
{
  vec3 r = { { v.v[0] * m, v.v[1] * m, v.v[2] * m } };
  return r;
}

static inline vec3 operator+(const vec3 &v, const vec3 &m) noexcept
{
  vec3 r = { { v.v[0] + m.v[0], v.v[1] + m.v[1], v.v[2] + m.v[2] } };
  return r;
}

static inline vec3 operator-(const vec3 &v, const vec3 &m) noexcept
{
  vec3 r = { { v.v[0] - m.v[0], v.v[1] - m.v[1], v.v[2] - m.v[2] } };
  return r;
}

static inline vec3 operator/(const vec3 &v, const vec3 &m) noexcept
{
  vec3 r = { { v.v[0] / m.v[0], v.v[1] / m.v[1], v.v[2] / m.v[2] } };
  return r;
}

static inline vec3 mix(const vec3 &a, const vec3 &b, const float &m) noexcept
{
  return a + (b - a) * m;
}

// colours

struct colour { vec3 v; };
struct linearRGB { vec3 v; };
struct sRGB { vec3 v; };
struct sRGB8 { unsigned char v[3]; };

static inline colour blend(const colour &a, const colour &b, const float &t) noexcept
{
  // geometric interpolation of chroma to preserve saturation
  std::complex<float> ca(a.v.v[0], a.v.v[1]);
  std::complex<float> cb(b.v.v[0], b.v.v[1]);
  cb = std::log(cb / ca);
  ca = std::log(ca);
  std::complex<float> cc = ca + cb * t;
  cc = std::exp(cc);
  colour c =
    { { { std::real(cc), std::imag(cc)
        , a.v.v[2] + (b.v.v[2] - a.v.v[2]) * t } } };
  // linear interpolation of chroma without problems at opposite chroma
  colour d;
  d.v = mix(a.v, b.v, t);
  // use cosine of chroma angle to blend between geometric and linear
  float s =      (a.v.v[0] * b.v.v[0] + a.v.v[1] * b.v.v[1])
      / std::sqrt(a.v.v[0] * a.v.v[0] + a.v.v[1] * a.v.v[1])
      / std::sqrt(b.v.v[0] * b.v.v[0] + b.v.v[1] * b.v.v[1]);
  colour e;
  e.v = mix(c.v, d.v, (1 - s) / 2);
  return e;
}

// Catmull-Rom spline

struct vec4 { float v[4]; };
struct mat4 { float m[4][4]; };
struct mat4x3 { vec3 m[4]; };

static inline vec4 operator*(const vec4 &a, const mat4 &b) noexcept
{
  vec4 r = { { 0, 0, 0, 0 } };
  for (int i = 0; i < 4; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      r.v[i] += a.v[j] * b.m[j][i];
    }
  }
  return r;
}

static inline vec3 operator*(const vec4 &a, const mat4x3 &b) noexcept
{
  vec3 r = { { 0, 0, 0 } };
  for (int i = 0; i < 3; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      r.v[i] += a.v[j] * b.m[j].v[i];
    }
  }
  return r;
}

static inline colour blend(const colour &a, const colour &b, const colour &c, const colour &d, const float &t) noexcept
{
  vec4 u = { { 1, t, t * t, t * t * t } };
  mat4 M = { { {  0,  2,  0,  0 }
             , { -1,  0,  1,  0 }
             , {  2, -5,  4, -1 }
             , { -1,  3, -3,  1 } } };
  mat4x3 p = { { a.v, b.v, c.v, d.v } };
  colour o = { u * M * p * 0.5 };
  return o;
}

static inline linearRGB to_linearRGB(const colour &a) noexcept
{
  const linearRGB grey   = { { { 0.5f, 0.5f, 0.5f } } };
  const linearRGB red    = { { { 1, 0, 0 } } };
  const linearRGB green  = { { { 0, 1, 0 } } };
  const linearRGB yellow = { { { 1, 1, 0 } } };
  const linearRGB blue   = { { { 0, 0, 1 } } };
  float r = std::hypot(a.v.v[0], a.v.v[1]);
  float s = std::tanh(r) / r;
  float u = s * a.v.v[0];
  float v = s * a.v.v[1];
  float l = std::exp2(a.v.v[2]);
  linearRGB c =
    { mix( u < 0 ? mix(grey.v, green.v, -u) : mix(grey.v, red.v, u)
         , v < 0 ? mix(grey.v, blue.v, -v) : mix(grey.v, yellow.v, v)
         , 0.5f) * l };
  return c;
}

static inline float to_sRGB(float l) noexcept
{
  l = std::min(std::max(l, 0.0f), 1.0f);
  if (l <= 0.0031308f)
    return l * 12.92f;
  return 1.055f  * pow(l, 1.0f / 2.4f) - 0.055f;
}

static inline sRGB to_sRGB(const linearRGB &a) noexcept
{
  sRGB c;
  c.v.v[0] = to_sRGB(a.v.v[0]);
  c.v.v[1] = to_sRGB(a.v.v[1]);
  c.v.v[2] = to_sRGB(a.v.v[2]);
  return c;
}

static inline sRGB8 to_sRGB8(const sRGB &a) noexcept
{
  // TODO dither
  sRGB8 c;
  c.v[0] = std::min(std::max(std::round(255.0f * a.v.v[0]), 0.0f), 255.0f);
  c.v[1] = std::min(std::max(std::round(255.0f * a.v.v[1]), 0.0f), 255.0f);
  c.v[2] = std::min(std::max(std::round(255.0f * a.v.v[2]), 0.0f), 255.0f);
  return c;
}

#define MAX_COLOURS 8

struct wave
{
  float offset;
  float period;
  int ncolours;
  colour colours[MAX_COLOURS];
};

static inline colour wave_colour(const wave &w, double n) noexcept
{
  double m = w.offset + n / w.period;
  int k = std::floor(m);
  float t = m - k;
  k -= 1;
  k %= w.ncolours;
  k += w.ncolours; // in case of negatives
  k %= w.ncolours;
  return blend
    ( w.colours[(k    )             ]
    , w.colours[(k + 1) % w.ncolours]
    , w.colours[(k + 2) % w.ncolours]
    , w.colours[(k + 3) % w.ncolours]
    , t
    );
}

#define MAX_WAVES 8

struct multiwave
{
  int nwaves;
  linearRGB grey_point;
  float blend[MAX_WAVES];
  struct wave waves[MAX_WAVES];
};

static inline colour multiwave_colour(const multiwave &w, double n) noexcept
{
  colour c = { { 1e-6, 1e-6, 0 } };
  for (int k = w.nwaves - 1; k >= 0; --k)
  {
    c = blend(c, wave_colour(w.waves[k], n), w.blend[k]);
  }
  return c;
}

struct palette
{
  multiwave n;
  multiwave s;
  float blend_ns;
  float slope_x;
  float slope_y;
  float slope_power;
  colour slope_light;
  colour slope_dark;
  colour slope_grey;
  float blend_slope;
  int stripe_power;
  float stripe_decay;
  bool parity;
};


static inline colour palette_colour(const palette &p, double n, double s, double x, double y) noexcept
{
  double z = x * x + y * y;
  x =  x / z;
  y = -y / z;
  double d = p.slope_power * (x * p.slope_x + y * p.slope_y);
  colour cslope;
  if (d >= 0)
  {
    d = std::atan(d) / (3.141592653589793 / 2);
    cslope = p.slope_dark;
  }
  else
  {
    d = std::atan(-d) / (3.141592653589793 / 2);
    cslope = p.slope_light;
  }
  cslope.v = cslope.v / p.slope_grey.v;
  colour cstripe = multiwave_colour(p.s, s);
  cstripe.v = cstripe.v / p.s.grey_point.v;
  colour citer   = multiwave_colour(p.n, n);
  citer.v = citer.v / p.n.grey_point.v;
  return blend
    (blend(citer, cstripe, p.blend_ns), cslope, p.blend_slope * d);
}

// fractal formulas

struct formula_group
{
  int repeats;
  int power;
  bool abs_x;
  bool abs_y;
  bool neg_x;
  bool neg_y;
};

#define FORMULA_GROUPS_MAX 8

struct formula
{
  int ngroups;
  int loop_start;
  formula_group group[FORMULA_GROUPS_MAX];
};

// explore worker

struct explore_input
{
  uint32_t seed;
  bool verbose;
  int quality;
};

struct explore_output
{
  formula f;
  int maxiters[64];
  coordinates end;
  mat2 skew;
  palette pal;
  uint32_t seed;
};

extern explore_output explore1(const explore_input &in);

// render worker

struct render_input
{
  formula f;
  int maxiters0;
  int maxiters1;
  coordinates coords;
  mat2 skew;
  palette pal;
  int quality;
  int aa;
  bool verbose;
  uint32_t seed;
  int k;
};

struct render_output
{
  size_t size;
  uint32_t seed;
  int k;
  unsigned char data[];
};

extern render_output *render1(const render_input &in);

#ifndef __EMSCRIPTEN__
extern struct work explore(struct work job);
extern struct work render(struct work job);
#endif
