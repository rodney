#!/bin/bash
# rodney -- fractal zoom explorer bot
# Copyright (C) 2021 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-or-later
echo "/* machine-generated file, do not edit */"
echo "const char *$1 ="
sed 's|//.*||' |
sed 's|^|"|' |
sed 's|$|\\n"|'
echo ";"
