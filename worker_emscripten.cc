// rodney -- fractal zoom explorer bot
// Copyright (C) 2020 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

#include "worker.h"

#include <emscripten/emscripten.h>

#include <list>
#include <string>
#include <vector>

struct workerpool
{
  std::vector<worker_handle> workers;
  int next;
  std::string name;
  std::list<struct work> results;
};

struct workerpool *workerpool_new(int threads, const char *url, const char *name)
{
  struct workerpool *pool = new workerpool();
  for (int i = 0; i < threads; ++i)
  {
    pool->workers.push_back(emscripten_create_worker(url));
  }
  pool->next = 0;
  pool->name = name;
  return pool;
}

void workerpool_callback(char *data, int size, void *arg)
{
  struct workerpool *pool = (struct workerpool *) arg;
  struct work result = { 0, 0 };
  if (data && size > 0)
  {
    result.data = (char *) std::malloc(size);
    std::memcpy(result.data, data, size);
    result.size = size;
  }
  pool->results.push_back(result);
}

void workerpool_delete(struct workerpool *pool) // FIXME
{
  (void) pool;
}

void workerpool_put(struct workerpool *pool, struct work job)
{
  emscripten_call_worker(pool->workers[pool->next], pool->name.c_str(), job.data, job.size, workerpool_callback, pool);
  pool->next++;
  if (pool->next >= (int) pool->workers.size())
  {
    pool->next = 0;
  }
}

bool workerpool_empty(struct workerpool *pool)
{
  return pool->results.empty();
}

struct work workerpool_get(struct workerpool *pool)
{
  struct work result = pool->results.front();
  pool->results.pop_front();
  return result;
}
