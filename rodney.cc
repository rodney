// rodney -- fractal zoom explorer bot
// Copyright (C) 2020,2021 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

#ifdef __EMSCRIPTEN__
#include <GLES2/gl2.h>
#include <EGL/egl.h>
#include <emscripten/emscripten.h>
#include <emscripten/html5.h>
#else
#define GLFW_INCLUDE_NONE
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#endif

#include <cstdio>
#include <iomanip>
#include <map>
#include <string>
#include <sstream>
#include <vector>

#include "rodney.h"
#include "worker.h"

const char *vert_source =
"attribute vec2 v_in;\n"
"varying vec2 texcoord;\n"
"uniform vec3 u_dims;\n"
"void main()\n"
"{\n"
"  texcoord = vec2(v_in.x * u_dims.z, v_in.y);\n"
"  gl_Position = vec4(mix(-1.0, 1.0, v_in.x), mix(-1.0, 1.0, mix(u_dims.x, u_dims.y, v_in.y)), 0.0, 1.0);\n"
"}\n"
;

const char *frag_source =
"precision mediump float;\n"
"varying vec2 texcoord;\n"
"uniform sampler2D u_tex;\n"
"void main()\n"
"{\n"
"  gl_FragColor = texture2D(u_tex, texcoord);\n"
"}\n"
;

struct
{
  std::vector<GLuint> active_textures;
  std::vector<GLuint> spare_textures;

  uint32_t next_seed;
  uint32_t current_seed;
  int current_frame;
  std::map<uint32_t, int> frame_count;
  std::map<std::pair<uint32_t, int>, std::vector<unsigned char>> frame_queue;

  // exponential map tile renderers
  struct workerpool *renderers;
  int renders_in_progress;
  int render_quality;

  // exploration intelligence
  struct workerpool *explorers;
  int explores_in_progress;
  int explore_quality;

  bool oneshot;
  int verbose;
  int antialiasing;
  double scroll_y;
  double scroll_speed;

  GLint u_dims;

#ifndef __EMSCRIPTEN__
  GLFWwindow *window;
#endif
} state;

static void render_done(struct work result)
{
  --state.renders_in_progress;
  int render_bytes = (1 << state.render_quality) * (1 << (state.render_quality - 3)) * 3;
  if (result.data == nullptr || result.size != (int) (sizeof(render_output) + render_bytes))
  {
    return;
  }
  render_output *out = (render_output *) result.data;
  std::vector<unsigned char> o;
  o.assign(out->data, out->data + render_bytes);
  state.frame_queue.insert(std::pair<std::pair<uint32_t, int>, std::vector<unsigned char>>(std::pair<uint32_t, int>(out->seed, out->k), o));
  std::free(out);
  if (state.oneshot)
  {
    if (state.renders_in_progress == 0)
    {
      exit(0);
    }
  }
}

static void explore_done(struct work result)
{
  if (result.data == nullptr || result.size != sizeof(explore_output))
  {
    return;
  }
  explore_output *out = (explore_output *) result.data;
  uint32_t seed = out->seed;
  --state.explores_in_progress;
  coordinates co = { out->end.cx, out->end.cy, 4 * std::pow(eigenvalue_ratio(out->skew), 2) };
  int koff = 0;
  int k = 0;
#ifdef RODNEY_SAVE_RAW
  koff = 5;
  co.r *= (1 << koff);
#endif
  while (co.r > out->end.r)
  {
    render_input in = { out->f, out->maxiters[std::min(std::max(k - koff, 0), 63)], out->maxiters[std::min(std::max(k + 1 - koff, 0), 63)], co, out->skew, out->pal, state.render_quality, state.antialiasing, bool(state.verbose & 2), seed, k };
    struct work job = { (char *) &in, sizeof(render_input) };
    workerpool_put(state.renderers, job);
    ++state.renders_in_progress;
    co.r /= 2;
    ++k;
  }
  state.frame_count.insert(std::pair<uint32_t, int>(seed, k));
}

static bool is_all_white(const std::vector<unsigned char> &data)
{
  bool white = true;
  for (auto j = data.begin(); j != data.end(); ++j)
  {
    white = white && *j == 255;
    if (! white)
    {
      break;
    }
  }
  return white;
}

static bool is_all_black(const std::vector<unsigned char> &data)
{
  bool black = true;
  for (auto j = data.begin(); j != data.end(); ++j)
  {
    black = black && *j == 0;
    if (! black)
    {
      break;
    }
  }
  return black;
}

static bool process_frame_queue(void)
{
  bool processed = false;
  auto fcit = state.frame_count.find(state.current_seed);
  if (fcit != state.frame_count.end())
  {
    int frame_count = fcit->second;
    if (state.current_frame < frame_count)
    {
      std::pair<int, int> ix = std::pair<int, int>(state.current_seed, state.current_frame);
      auto current = state.frame_queue.find(ix);
      if (current != state.frame_queue.end())
      {
        int render_width = 1 << state.render_quality;
        int render_height = 1 << (state.render_quality - 3);
        std::vector<unsigned char> data = current->second;
        state.frame_queue.erase(current);
        state.current_frame++;
        if (! is_all_white(data) && ! is_all_black(data))
        {
#if 0
          std::ostringstream filename;
          filename << "out/" << state.current_seed << "_" << std::setw(4) << std::setfill('0') << (state.current_frame - 1) << ".ppm";
          std::FILE *out = std::fopen(filename.str().c_str(), "wb");
          std::fprintf(out, "P6\n%d %d\n255\n", render_width, render_height);
          std::fwrite(&data[0], (size_t) render_width * render_height * 3, 1, out);
          std::fclose(out);
#endif
          GLuint tex = 0;
          if (state.spare_textures.size() > 0)
          {
            tex = state.spare_textures.back();
            state.spare_textures.pop_back();
          }
          if (! tex)
          {
            glGenTextures(1, &tex);
            glActiveTexture(GL_TEXTURE0 + 0);
            glBindTexture(GL_TEXTURE_2D, tex);
            glTexImage2D
              ( GL_TEXTURE_2D, 0, GL_RGB, render_width, render_height, 0
              , GL_RGB, GL_UNSIGNED_BYTE, &data[0]
              );
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
          }
          else
          {
            glActiveTexture(GL_TEXTURE0 + 0);
            glBindTexture(GL_TEXTURE_2D, tex);
            glTexSubImage2D
              ( GL_TEXTURE_2D, 0, 0, 0, render_width, render_height
              , GL_RGB, GL_UNSIGNED_BYTE, &data[0]
              );
          }
          state.active_textures.push_back(tex);
        }
        processed = true;
      }
    }
    else
    {
      // finished all frames for this seed, switch waiting to next
      state.frame_count.erase(fcit);
      ++state.current_seed;
      state.current_frame = 0;
      // spawn another explorer
      if (! state.oneshot)
      {
        explore_input in = { state.next_seed++, bool(state.verbose & 1), state.explore_quality };
        struct work job = { (char *) &in, sizeof(explore_input) };
        workerpool_put(state.explorers, job);
        ++state.explores_in_progress;
      }
    }
  }
  else
  {
    // no frame count for current seed
    // probably the first explorer is not yet done
    // ignore
  }
  return processed;
}

static void scroll(int needed_textures)
{
  int render_width = 1 << state.render_quality;
  int render_height = 1 << (state.render_quality - 3);
  double decay = std::pow(0.5 / render_width, 0.5 / render_height);
  double scroll_speed_target
    = needed_textures + 2 < (int) state.active_textures.size();
  state.scroll_speed
    = state.scroll_speed * decay + (1.0 - decay) * scroll_speed_target;
  state.scroll_y += state.scroll_speed;
  while (state.scroll_y >= render_height)
  {
    if (state.active_textures.size() > 0)
    {
      GLuint tex = state.active_textures.front();
      state.active_textures.erase(state.active_textures.begin());
      state.spare_textures.push_back(tex);
    }
    state.scroll_y -= render_height;
  }
}

static int draw(void)
{
  int render_width = 1 << state.render_quality;
  int render_height = 1 << (state.render_quality - 3);
  int width = 1920;
  int height = 1080;
#ifdef __EMSCRIPTEN__
  if (emscripten_get_canvas_element_size("#canvas", &width, &height) != EMSCRIPTEN_RESULT_SUCCESS)
  {
    // failsafe
    width = 1280;
    height = 720;
  }
#else
  glfwGetFramebufferSize(state.window, &width, &height);
#endif
  int h = (height + render_height - 1) / render_height;
  double w = width / (double) render_width;
  glClear(GL_COLOR_BUFFER_BIT);
  for (int j = 0; j <= h; ++j)
  {
    if (j < (int) state.active_textures.size())
    {
      glBindTexture(GL_TEXTURE_2D, state.active_textures[j]);
    }
    else
    {
      glBindTexture(GL_TEXTURE_2D, 0);
    }
    double y1 = j * render_height - state.scroll_y;
    double y2 = y1 + render_height;
    glUniform3f(state.u_dims, y1 / height, y2 / height, w);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  }
  return h + 1;
}

void main1(void)
{
  while (! workerpool_empty(state.explorers))
  {
    explore_done(workerpool_get(state.explorers));
  }
  while (! workerpool_empty(state.renderers))
  {
    render_done(workerpool_get(state.renderers));
  }
  while (process_frame_queue())
  {
    // nop
  }
  int needed_textures = draw();
  scroll(needed_textures);
#ifdef __EMSCRIPTEN__
  // eglSwapBuffers(state.display, state.surface); // not necessary for web
#else
  glfwSwapBuffers(state.window);
#endif
}

int main(int argc, char **argv)
{
  std::fprintf(stdout, "rodney (C) 2020,2021 Claude Heiland-Allen\n");

#ifdef __EMSCRIPTEN__
  {
    EGLDisplay display = eglGetDisplay(0);
    if (eglInitialize(display, 0, 0) != EGL_TRUE)
    {
      std::fprintf(stderr, "eglInitialize failed: %d\n", eglGetError());
      std::abort();
    }
    EGLint count = 0;
    if (eglGetConfigs(display, 0, 0, &count) != EGL_TRUE)
    {
      std::fprintf(stderr, "eglGetConfigs failed: %d\n", eglGetError());
      std::abort();
    }
    if (count <= 0)
    {
      std::fprintf(stdout, "%d EGLConfigs found\n", count);
      std::abort();
    }
    EGLConfig *configs = (EGLConfig *) std::calloc(count, sizeof(*configs));
    if (! configs)
    {
      std::abort();
    }
    if (eglGetConfigs(display, configs, count, &count) != EGL_TRUE)
    {
      std::fprintf(stderr, "eglGetConfigs failed: %d\n", eglGetError());
      std::abort();
    }
    EGLSurface surface = eglCreateWindowSurface(display, configs[0], 0, 0);
    if (surface == EGL_NO_SURFACE)
    {
      std::fprintf(stderr, "eglCreateWindowSurface failed: %d\n", eglGetError());
      std::abort();
    }
    EGLint attribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE, EGL_NONE };
    EGLContext context = eglCreateContext(display, configs[0], EGL_NO_CONTEXT, attribs);
    if (context == EGL_NO_CONTEXT)
    {
      std::fprintf(stderr, "eglCreateContext failed: %d\n", eglGetError());
      std::abort();
    }
    if (eglMakeCurrent(display, surface, surface, context) != EGL_TRUE)
    {
      std::fprintf(stderr, "eglMakeCurrent failed: %d\n", eglGetError());
      std::abort();
    }
  }
#else
  {
    if (! glfwInit())
    {
      std::fprintf(stderr, "error: could not initialize glfw\n");
      return 1;
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);
    state.window = glfwCreateWindow(1920, 1080, "Rodney, you plonker!", 0, 0);
    if (! state.window)
    {
      std::fprintf(stderr, "error: could not create glfw window\n");
      glfwTerminate();
      return 1;
    }
    glfwMakeContextCurrent(state.window);
    if (! gladLoadGLES2Loader((GLADloadproc) glfwGetProcAddress))
    {
      std::fprintf(stderr, "error: could not load GLES2\n");
      glfwTerminate();
      return 1;
    }
  }
#endif

  int seed = time(0);
  int threads = 1;
  int verbose = 0;
  int antialiasing = 3;
  int explore_quality = 8;
  int render_quality = 9;
  bool oneshot = false;
  for (int arg = 1; arg < argc; ++arg)
  {
    if (std::string(argv[arg]) == "--antialiasing" || std::string(argv[arg]) == "--aa")
    {
      ++arg;
      if (arg < argc)
      {
        antialiasing = std::min(std::max(std::atoi(argv[arg]), 1), 8);
      }
    }
    if (std::string(argv[arg]) == "--explore-quality")
    {
      ++arg;
      if (arg < argc)
      {
        explore_quality = std::min(std::max(std::atoi(argv[arg]), 5), 12);
      }
    }
    if (std::string(argv[arg]) == "--one-shot" || std::string(argv[arg]) == "-1")
    {
      oneshot = true;
    }
    if (std::string(argv[arg]) == "--render-quality")
    {
      ++arg;
      if (arg < argc)
      {
        render_quality = std::min(std::max(std::atoi(argv[arg]), 5), 14);
      }
    }
    if (std::string(argv[arg]) == "--seed")
    {
      ++arg;
      if (arg < argc)
      {
        seed = std::atoi(argv[arg]);
      }
    }
    if (std::string(argv[arg]) == "--threads")
    {
      ++arg;
      if (arg < argc)
      {
        threads = std::max(1, std::atoi(argv[arg]));
      }
    }
    if (std::string(argv[arg]) == "--verbose")
    {
      ++arg;
      if (arg < argc)
      {
        verbose = std::atoi(argv[arg]);
      }
    }
  }

  // TODO adapt to window size
  int width = 1920;
  int height = 1080;
#ifdef __EMSCRIPTEN__
  if (emscripten_get_canvas_element_size("#canvas", &width, &height) != EMSCRIPTEN_RESULT_SUCCESS)
  {
    std::fprintf(stderr, "error: couldn't get canvas element size\n");
    // failsafe
    width = 1280;
    height = 720;
  }
#else
  glfwGetFramebufferSize(state.window, &width, &height);
#endif
  std::fprintf(stdout, "using canvas size %d x %d\n", width, height);
  state.render_quality = render_quality;
  state.oneshot = oneshot;
  state.verbose = verbose;
  state.antialiasing = antialiasing;
  state.scroll_y = 0;
  state.scroll_speed = 0;

  glClearColor(0.5, 0.5, 0.5, 1);

  // allocate blank screen for smooth start
  int render_width = 1 << state.render_quality;
  int render_height = 1 << (state.render_quality - 3);
  int render_bytes = render_width * render_height * 3;
  int h = (height + render_width - 1) / render_height;
  std::vector<unsigned char> white(render_bytes);
  std::fill(white.begin(), white.end(), 0xFF);
  for (int j = 0; j <= h; ++j)
  {
    GLuint tex = 0;
    glGenTextures(1, &tex);
    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D
      ( GL_TEXTURE_2D, 0, GL_RGB, render_width, render_height, 0
      , GL_RGB, GL_UNSIGNED_BYTE, &white[0]
      );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    state.active_textures.push_back(tex);
  }

  {
    // compile shader
    GLuint vert = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vert, 1, &vert_source, nullptr);
    glCompileShader(vert);
    GLint compiled = GL_FALSE;
    glGetShaderiv(vert, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE)
    {
      GLsizei log_length = 0;
      GLchar message[1024];
      glGetShaderInfoLog(vert, 1024, &log_length, message);
      std::fprintf(stdout, "vertex shader error:\n%s\n", message);
      std::abort();
    }
    GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(frag, 1, &frag_source, nullptr);
    glCompileShader(frag);
    compiled = GL_FALSE;
    glGetShaderiv(frag, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE)
    {
      GLsizei log_length = 0;
      GLchar message[1024];
      glGetShaderInfoLog(frag, 1024, &log_length, message);
      std::fprintf(stdout, "fragment shader error:\n%s\n", message);
      std::abort();
    }
    GLuint prog = glCreateProgram();
    glBindAttribLocation(prog, 0, "v_in");
    glAttachShader(prog, vert);
    glAttachShader(prog, frag);
    glLinkProgram(prog);
    GLint linked = GL_FALSE;
    glGetProgramiv(prog, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE)
    {
      GLsizei log_length = 0;
      GLchar message[1024];
      glGetProgramInfoLog(prog, 1024, &log_length, message);
      std::fprintf(stdout, "shader link error:\n%s\n", message);
      std::abort();
    }
    glUseProgram(prog);
    glUniform1i(glGetUniformLocation(prog, "u_tex"), 0);
    state.u_dims = glGetUniformLocation(prog, "u_dims");
    // vertex data specification
    GLfloat data[] = { 0, 0, 1, 0, 0, 1, 1, 1 };
    GLuint buf = 0;
    glGenBuffers(1, &buf);
    glBindBuffer(GL_ARRAY_BUFFER, buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
  }

  // spawn workers
  int EXPLORERS = 1;
  int RENDERERS = std::min(std::max(threads - EXPLORERS, 1), 64); // FIXME arbitrary upper limit
  std::fprintf(stdout, "using %d explore workers and %d render workers\n", EXPLORERS, RENDERERS);
#ifdef __EMSCRIPTEN__
  state.explorers = workerpool_new(EXPLORERS, "explore.js", "explore");
  state.renderers = workerpool_new(RENDERERS, "render.js", "render");
#else
  state.explorers = workerpool_new(EXPLORERS, explore);
  state.renderers = workerpool_new(RENDERERS, render);
#endif

  // launch explorers
  state.explore_quality = explore_quality;
  state.current_seed = seed;
  int k;
  for (k = 0; k < EXPLORERS; ++k)
  {
    explore_input in = { (uint32_t)(seed + k), bool(state.verbose & 1), state.explore_quality };
    struct work job = { (char *) &in, sizeof(explore_input) };
    workerpool_put(state.explorers, job);
    ++state.explores_in_progress;
  }
  state.next_seed = seed + k;

#ifdef __EMSCRIPTEN__
  emscripten_set_main_loop(main1, 0, 1);
#else
  while (! glfwWindowShouldClose(state.window))
  {
    main1();
    glfwPollEvents();
  }
#endif
  return 0;
}
