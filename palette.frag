// rodney -- fractal zoom explorer bot
// Copyright (C) 2021 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

// colouring algorithms

vec2 conj(vec2 a)
{
  return vec2(a.x, -a.y);
}

vec2 cmul(vec2 a, vec2 b)
{
  return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

vec2 cdiv(vec2 a, vec2 b)
{
  return cmul(a, conj(b)) / dot(b, b);
}

vec2 clog(vec2 a)
{
  return vec2(log(length(a)), atan(a.y, a.x));
}

vec2 cexp(vec2 a)
{
  return exp(a.x) * vec2(cos(a.y), sin(a.y));
}

float norm(vec3 v)
{
  return dot(v, v);
}

vec2 safe_normalize(vec2 v)
{
  float l = length(v);
  if (l > 0.0)
  {
    return v / l;
  }
  else
  {
    return v; // zero or NaN
  }
}

vec3 safe_normalize(vec3 v)
{
  float l = length(v);
  if (l > 0.0)
  {
    return v / l;
  }
  else
  {
    return v; // zero or NaN
  }
}

vec3 blend(vec3 a, vec3 b, float t)
{
  // geometric interpolation of chroma to preserve saturation
  vec2 ca = a.xy;
  vec2 cb = b.xy;
  cb = clog(cdiv(cb, ca));
  ca = clog(ca);
  vec2 cc = ca + cb * t;
  cc = cexp(cc);
  vec3 c = vec3(cc, mix(a.z, b.z, t));
  // linear interpolation of chroma without problems at opposite chroma
  vec3 d = mix(a, b, t);
  // use cosine of chroma angle to blend between geometric and linear
  float s = dot(safe_normalize(a.xy), safe_normalize(b.xy));
  vec3 e = mix(c, d, (1.0 - s) / 2.0);
  return e;
}

// Catmull-Rom spline

vec3 blend(vec3 a, vec3 b, vec3 c, vec3 d, float t)
{
  vec4 u = vec4(1.0, t, t * t, t * t * t);
  mat4 M = mat4(  0.0,  2.0,  0.0,  0.0
               , -1.0,  0.0,  1.0,  0.0
               ,  2.0, -5.0,  4.0, -1.0
               , -1.0,  3.0, -3.0,  1.0
               );
  mat3x4 p = mat3x4(a, b, c, d);
  vec3 o = u * M * p * 0.5;
  return o;
}

vec3 to_linearRGB(vec3 a)
{
  const vec3 grey   = vec3(0.5, 0.5, 0.5);
  const vec3 red    = vec3(1.0, 0.0, 0.0);
  const vec3 green  = vec3(0.0, 1.0, 0.0);
  const vec3 yellow = vec3(1.0, 1.0, 0.0);
  const vec3 blue   = vec3(0.0, 0.0, 1.0);
  float r = length(a.xy);
  float s = tanh(r) / r;
  float u = s * a.x;
  float v = s * a.y;
  float l = exp2(a.z);
  vec3 c =
    mix( u < 0.0 ? mix(grey, green, -u) : mix(grey, red, u)
       , v < 0.0 ? mix(grey, blue, -v) : mix(grey, yellow, v)
       , 0.5) * l;
  return c;
}

#define MAX_COLOURS 8

struct wave
{
  float offset;
  float period;
  int ncolours;
  vec3 colours[MAX_COLOURS];
};

vec3 wave_colour(wave w, double n)
{
  double m = w.offset + n / w.period;
  int k = int(floor(m));
  float t = float(m - k);
  k -= 1;
  k %= w.ncolours;
  k += w.ncolours; // in case of negatives
  k %= w.ncolours;
  return blend
    ( w.colours[(k    )             ]
    , w.colours[(k + 1) % w.ncolours]
    , w.colours[(k + 2) % w.ncolours]
    , w.colours[(k + 3) % w.ncolours]
    , t
    );
}

#define MAX_WAVES 8

struct multiwave
{
  int nwaves;
  vec3 grey_point;
  float blend[MAX_WAVES];
  wave waves[MAX_WAVES];
};

vec3 multiwave_colour(multiwave w, double n)
{
  vec3 c = vec3(1.0e-6, 1.0e-6, 0);
  for (int k = w.nwaves - 1; k >= 0; --k)
  {
    c = blend(c, wave_colour(w.waves[k], n), w.blend[k]);
  }
  return c;
}

struct palette
{
  multiwave n;
  multiwave s;
  float blend_ns;
  float slope_x;
  float slope_y;
  float slope_power;
  vec3 slope_light;
  vec3 slope_dark;
  vec3 slope_grey;
  float blend_slope;
  int stripe_power;
  float stripe_decay;
  bool parity;
};

vec3 palette_colour(palette p, double n, double s, float x, float y)
{
  float z = x * x + y * y;
  x =  x / z;
  y = -y / z;
  float d = p.slope_power * (x * p.slope_x + y * p.slope_y);
  vec3 cslope;
  if (d >= 0.0)
  {
    d = atan(d) / (3.141592653589793 / 2.0);
    cslope = p.slope_dark;
  }
  else
  {
    d = atan(-d) / (3.141592653589793 / 2.0);
    cslope = p.slope_light;
  }
  cslope = cslope / p.slope_grey;
  vec3 cstripe = multiwave_colour(p.s, double(s));
  cstripe = cstripe / p.s.grey_point;
  vec3 citer   = multiwave_colour(p.n, n);
  citer = citer / p.n.grey_point;
  return blend
    (blend(citer, cstripe, p.blend_ns), cslope, p.blend_slope * d);
}

vec4 colour(palette P, vec4 XYZT, uint N, vec2 NFT, vec2 DE, vec3 RGB)
{
  if (N == 0xFFffFFffu) return vec4(vec3(P.parity ? 0.0 : 1.0), 1.0);
  double n = double(N) + double(NFT.x);
  double avg = double(NFT.y);
  float d = length(DE) * pow(2.0, -min(XYZT.z - 5.0, 0.0));
  float val = min(d, 1.0);
  const float width = 320.0;
  float desat = min(pow(min(d / width, 1.0), 4.0), 1.0);
  vec3 c = palette_colour(P, 16.0 * n, 4.0 * avg, DE.x, DE.y);
  return vec4(mix(vec3(P.parity ? 0.0 : 1.0), mix(to_linearRGB(c), vec3(P.parity ? 1.0 : 0.0), desat), val), 1.0);
}

//const float nan = 0.0;

