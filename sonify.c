// rodney -- fractal zoom explorer bot
// Copyright (C) 2021 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

/*
// http://lolengine.net/blog/2013/07/27/rgb-to-hsv-in-glsl
vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = c.g < c.b ? vec4(c.bg, K.wz) : vec4(c.gb, K.xy);
    vec4 q = c.r < p.x ? vec4(p.xyw, c.r) : vec4(c.r, p.yzx);
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}
*/

#include <tgmath.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sndfile.h>

#define CL_TARGET_OPENCL_VERSION 300
#include <CL/opencl.h>

#include "sonify.h"

#define width 160
#define height 90
#define step 1
const char header[] = "P6\n160 90\n255\n";
unsigned char input[height][width][3];

#define twopi 6.283185307179586f
#define SR 44100
#define FPS 30
#define BLOCK (SR/FPS)
#define BF 2

typedef float sample;
typedef struct { sample buf[2][BLOCK/BF]; } DELAY20ms;
typedef struct { sample w[2][2][2]; } HILBERT;
typedef struct { sample re, im; } VCF;
typedef struct { sample y; } LOP;
struct pixel
{
  DELAY20ms del;
  HILBERT wave;
  VCF filter;
  LOP hz;
  LOP q;
  sample h_target;
  sample s_target;
};
    struct pixel cpu_state[height][width];

const char *error_string(int err) {
  switch (err) {
  case CL_SUCCESS:                            return 0;
  case CL_DEVICE_NOT_FOUND:                   return "Device not found.";
  case CL_DEVICE_NOT_AVAILABLE:               return "Device not available";
  case CL_COMPILER_NOT_AVAILABLE:             return "Compiler not available";
  case CL_MEM_OBJECT_ALLOCATION_FAILURE:      return "Memory object allocation failure";
  case CL_OUT_OF_RESOURCES:                   return "Out of resources";
  case CL_OUT_OF_HOST_MEMORY:                 return "Out of host memory";
  case CL_PROFILING_INFO_NOT_AVAILABLE:       return "Profiling information not available";
  case CL_MEM_COPY_OVERLAP:                   return "Memory copy overlap";
  case CL_IMAGE_FORMAT_MISMATCH:              return "Image format mismatch";
  case CL_IMAGE_FORMAT_NOT_SUPPORTED:         return "Image format not supported";
  case CL_BUILD_PROGRAM_FAILURE:              return "Program build failure";
  case CL_MAP_FAILURE:                        return "Map failure";
  case CL_INVALID_VALUE:                      return "Invalid value";
  case CL_INVALID_DEVICE_TYPE:                return "Invalid device type";
  case CL_INVALID_PLATFORM:                   return "Invalid platform";
  case CL_INVALID_DEVICE:                     return "Invalid device";
  case CL_INVALID_CONTEXT:                    return "Invalid context";
  case CL_INVALID_QUEUE_PROPERTIES:           return "Invalid queue properties";
  case CL_INVALID_COMMAND_QUEUE:              return "Invalid command queue";
  case CL_INVALID_HOST_PTR:                   return "Invalid host pointer";
  case CL_INVALID_MEM_OBJECT:                 return "Invalid memory object";
  case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:    return "Invalid image format descriptor";
  case CL_INVALID_IMAGE_SIZE:                 return "Invalid image size";
  case CL_INVALID_SAMPLER:                    return "Invalid sampler";
  case CL_INVALID_BINARY:                     return "Invalid binary";
  case CL_INVALID_BUILD_OPTIONS:              return "Invalid build options";
  case CL_INVALID_PROGRAM:                    return "Invalid program";
  case CL_INVALID_PROGRAM_EXECUTABLE:         return "Invalid program executable";
  case CL_INVALID_KERNEL_NAME:                return "Invalid kernel name";
  case CL_INVALID_KERNEL_DEFINITION:          return "Invalid kernel definition";
  case CL_INVALID_KERNEL:                     return "Invalid kernel";
  case CL_INVALID_ARG_INDEX:                  return "Invalid argument index";
  case CL_INVALID_ARG_VALUE:                  return "Invalid argument value";
  case CL_INVALID_ARG_SIZE:                   return "Invalid argument size";
  case CL_INVALID_KERNEL_ARGS:                return "Invalid kernel arguments";
  case CL_INVALID_WORK_DIMENSION:             return "Invalid work dimension";
  case CL_INVALID_WORK_GROUP_SIZE:            return "Invalid work group size";
  case CL_INVALID_WORK_ITEM_SIZE:             return "Invalid work item size";
  case CL_INVALID_GLOBAL_OFFSET:              return "Invalid global offset";
  case CL_INVALID_EVENT_WAIT_LIST:            return "Invalid event wait list";
  case CL_INVALID_EVENT:                      return "Invalid event";
  case CL_INVALID_OPERATION:                  return "Invalid operation";
  case CL_INVALID_GL_OBJECT:                  return "Invalid OpenGL object";
  case CL_INVALID_BUFFER_SIZE:                return "Invalid buffer size";
  case CL_INVALID_MIP_LEVEL:                  return "Invalid mip-map level";
  default: return "Unknown";
  }
}

void error_print(int err, int loc) {
  if (err == CL_SUCCESS) { return; }
  fprintf(stderr, "OpenCL error: %s at %d\n", error_string(err), loc);
  abort();
}

#define E(err) error_print(err, __LINE__)

int main(int argc, char **argv)
{
  if (argc < 2)
  {
    fprintf(stderr, "usage: %s < 320x180p30.ppm output.wav\n", argv[0]);
    return 1;
  }
  cl_platform_id platform_id[64];
  cl_uint platform_ids = 0;
  cl_int err = clGetPlatformIDs(64, &platform_id[0], &platform_ids);
  if (err != CL_SUCCESS)
  {
    return 1;
  }
  cl_device_id device_id[64][64];
  for (cl_uint i = 0; i < platform_ids; ++i) {
    char buf[1024];
    buf[0] = 0;
    E(clGetPlatformInfo(platform_id[i], CL_PLATFORM_VERSION, 1024, &buf[0], 0));
    buf[0] = 0;
    E(clGetPlatformInfo(platform_id[i], CL_PLATFORM_VENDOR, 1024, &buf[0], 0));
    fprintf(stderr, "VENDOR %d = %s\n", i, buf);
    cl_uint device_ids;
    E(clGetDeviceIDs(platform_id[i], CL_DEVICE_TYPE_ALL, 64, &device_id[i][0], &device_ids));
    for (cl_uint j = 0; j < device_ids; ++j)
    {
      buf[0] = 0;
      E(clGetDeviceInfo(device_id[i][j], CL_DEVICE_NAME, 1024, &buf[0], 0));
      fprintf(stderr, "    DEVICE %d = %s\n", j, buf);
    }
  }
  // create context
  cl_context_properties properties[] =
    {
      CL_CONTEXT_PLATFORM, (cl_context_properties) platform_id[0]
    , 0
    };
  cl_context context = clCreateContext(properties, 1, &device_id[0][0], NULL, NULL, &err);
  if (! context) { E(err); }
  cl_command_queue commands = clCreateCommandQueue(context, device_id[0][0], 0, &err);
  if (! commands) { E(err); }
  // compile kernels
  cl_program program = clCreateProgramWithSource(context, 1, &opencl, 0, &err);
  if (! program) { E(err); }
  err = clBuildProgram(program, 1, &device_id[0][0], "-cl-fast-relaxed-math", 0, 0);
  if (err != CL_SUCCESS) {
    char *buf = (char *) malloc(1000000);
    buf[0] = 0;
    E(clGetProgramBuildInfo(program, device_id[0][0], CL_PROGRAM_BUILD_LOG, 1000000, &buf[0], 0));
    fprintf(stderr, "%s\n", buf);
    free(buf);
    E(err);
  }
  cl_kernel analyze_input = clCreateKernel(program, "analyze_input", &err);
  if (! analyze_input) { E(err); }
  cl_kernel generate_audio = clCreateKernel(program, "generate_audio", &err);
  if (! generate_audio) { E(err); }
  cl_kernel accumulate_output = clCreateKernel(program, "accumulate_output", &err);
  if (! accumulate_output) { E(err); }
  // allocate memory
  size_t gain_bytes = width * 2 * sizeof(sample);
  cl_mem gain = clCreateBuffer(context, CL_MEM_READ_ONLY, gain_bytes, 0, &err);
  if (! gain) { E(err); }
  size_t input_bytes = width * height * 3;
  cl_mem input = clCreateBuffer(context, CL_MEM_READ_ONLY, input_bytes, 0, &err);
  if (! input) { E(err); }
  size_t state_bytes = width * height * sizeof(struct pixel);
  cl_mem state = clCreateBuffer(context, CL_MEM_READ_WRITE, state_bytes, 0, &err);
  if (! input) { E(err); }
  size_t buffer_bytes = width * height * BLOCK/BF * sizeof(sample);
  cl_mem buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, buffer_bytes, 0, &err);
  if (! buffer) { E(err); }
  size_t output_bytes = 2 * BLOCK/BF * sizeof(sample);
  cl_mem output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, output_bytes, 0, &err);
  if (! output) { E(err); }
  cl_event state_cleared;
  {
    memset(cpu_state, 0, sizeof(cpu_state));
    E(clEnqueueWriteBuffer(commands, state, CL_TRUE, 0, state_bytes, &cpu_state[0][0], 0, 0, &state_cleared));
  }
  cl_event gain_uploaded;
  {
    sample cpu_gain[width][2];
    for (int i = 0; i < width/step; ++i)
    {
      double t = (i + 0.5) / (width/step) * twopi / 4;
      cpu_gain[i][0] = 16 * cos(t) / (width * height) * step * step;
      cpu_gain[i][1] = 16 * sin(t) / (width * height) * step * step;
    }
    E(clEnqueueWriteBuffer(commands, gain, CL_TRUE, 0, gain_bytes, &cpu_gain[0][0], 1, &state_cleared, &gain_uploaded));
  }

  SF_INFO fi = { 0, SR, 2, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *file = sf_open(argv[1], SFM_WRITE, &fi);
  if (! file) return 1;

  int dr = 0, dw = 1;
  for (int frame = 0; true; ++frame)
  {
    unsigned char header_buffer[100];
    if (1 != fread(header_buffer, strlen(header), 1, stdin))
    {
      break;
    }
    cl_event input_uploaded;
    {
      unsigned char cpu_input[height][width][3];
      if (1 != fread(cpu_input, sizeof(cpu_input), 1, stdin))
      {
        return 1;
      }
      E(clEnqueueWriteBuffer(commands, input, CL_TRUE, 0, input_bytes, &cpu_input[0][0][0], 1, &gain_uploaded, &input_uploaded));
    }
    cl_event input_analysed;
    {
      cl_int widthi = width;
      cl_uint seed = frame * (width * height * 3);
      E(clSetKernelArg(analyze_input, 0, sizeof(cl_mem), &input));
      E(clSetKernelArg(analyze_input, 1, sizeof(cl_mem), &state));
      E(clSetKernelArg(analyze_input, 2, sizeof(cl_int), &widthi));
      E(clSetKernelArg(analyze_input, 3, sizeof(cl_uint), &seed));
      size_t global[2] = { (size_t) height, (size_t) width };
      E(clEnqueueNDRangeKernel(commands, analyze_input, 2, 0, global, 0, 1, &input_uploaded, &input_analysed));
    }
    for (int l = 0; l < BF; ++l)
    {
      cl_event audio_generated;
      {
        cl_int widthi = width;
        cl_int heighti = height;
        cl_int counti = BLOCK/BF;
        cl_int dri = dr;
        cl_uint seed = frame * BLOCK + l * BLOCK/BF;
        E(clSetKernelArg(generate_audio, 0, sizeof(cl_mem), &state));
        E(clSetKernelArg(generate_audio, 1, sizeof(cl_mem), &buffer));
        E(clSetKernelArg(generate_audio, 2, sizeof(cl_int), &widthi));
        E(clSetKernelArg(generate_audio, 3, sizeof(cl_int), &heighti));
        E(clSetKernelArg(generate_audio, 4, sizeof(cl_int), &counti));
        E(clSetKernelArg(generate_audio, 5, sizeof(cl_int), &dri));
        E(clSetKernelArg(generate_audio, 6, sizeof(cl_uint), &seed));
        size_t global[2] = { (size_t) height, (size_t) width };
        E(clEnqueueNDRangeKernel(commands, generate_audio, 2, 0, global, 0, 1, &input_analysed, &audio_generated));
      }
      cl_event output_accumulated;
      {
        cl_int widthi = width;
        cl_int heighti = height;
        cl_int counti = BLOCK/BF;
        E(clSetKernelArg(accumulate_output, 0, sizeof(cl_mem), &gain));
        E(clSetKernelArg(accumulate_output, 1, sizeof(cl_mem), &buffer));
        E(clSetKernelArg(accumulate_output, 2, sizeof(cl_mem), &output));
        E(clSetKernelArg(accumulate_output, 3, sizeof(cl_int), &widthi));
        E(clSetKernelArg(accumulate_output, 4, sizeof(cl_int), &heighti));
        E(clSetKernelArg(accumulate_output, 5, sizeof(cl_int), &counti));
        size_t global[1] = { counti };
        E(clEnqueueNDRangeKernel(commands, accumulate_output, 1, 0, global, 0, 1, &audio_generated, &output_accumulated));
      }
      cl_event output_downloaded;
      {
        sample cpu_output[BLOCK/BF][2];
        E(clEnqueueReadBuffer(commands, output, CL_TRUE, 0, sizeof(cpu_output), &cpu_output[0][0], 1, &output_accumulated, &output_downloaded));
        clWaitForEvents(1, &output_downloaded);
        sf_writef_float(file, &cpu_output[0][0], BLOCK/BF);
      }
      int tmp = dr; dr = dw; dw = tmp;
    }
  }
  sf_close(file);
  if (input) { clReleaseMemObject(input); input = 0; }
  if (state) { clReleaseMemObject(state); state = 0; }
  if (buffer) { clReleaseMemObject(buffer); buffer = 0; }
  if (output) { clReleaseMemObject(output); output = 0; }
  if (commands) { clReleaseCommandQueue(commands); commands = 0; }
  if (context) { clReleaseContext(context); context = 0; }
  return 0;
}
