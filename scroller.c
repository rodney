#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#undef DRYRUN
//#define DRYRUN

// IW >= OW
// OH % SPEED == 0
#define IW 2048
#define OW 1920
#define OH 1080
#define SPEED 6
// 1 2 3 4 5 6 8 9 10 12 15 18 20 24 27 30 36 40 45 54 60 72 90 108 120 135 180 216 270 360 540 1080

unsigned char ibuffer[IW][3];

char *oheader = "P6\n1920 1080\n255\n";
unsigned char obuffer[OH * 2][OW][3];

int main(int argc, char **argv)
{
  const int oheaderlen = strlen(oheader);
  const int obufferlen = OH * OW * 3;
  const int ii0 = (IW - OW) / 2;
  const int ii1 = ii0 + OW;
  const int scanlinelen = OW * 3;
  int oj = 0;
  int y = -OH;
  int scroll = 0;
  uint64_t frames = 0;
  for (int i = 1; i <= argc; ++i)
  {
    const char *filename = argv[i == argc ? 1 : i];
    fprintf(stderr, "NOTICE: opening '%s'\n", filename);
    FILE *ifile = fopen(filename, "rb");
    if (! ifile)
    {
      fprintf(stderr, "WARNING: could not open '%s'\n", filename);
      continue;
    }
    int iw = -1, ih = -1;
    if (2 != fscanf(ifile, "P6\n%d %d\n255", &iw, &ih))
    {
      fprintf(stderr, "WARNING: could not parse PPM header of '%s'\n", filename);
      fclose(ifile);
      continue;
    }
    if (iw != IW)
    {
      fprintf(stderr, "WARNING: width of '%s' is %d instead of %d\n", filename, iw, IW);
      fclose(ifile);
      continue;
    }
    if ('\n' != fgetc(ifile))
    {
      fprintf(stderr, "WARNING: could not parse PPM header of '%s'\n", filename);
      fclose(ifile);
      continue;
    }
    bool started = false;
    unsigned int ycount = 0;
    for (int ij = 0; ij < ih; ++ij)
    {
      if (1 != fread(ibuffer, sizeof(ibuffer), 1, ifile))
      {
        fprintf(stderr, "WARNING: could not read from '%s'\n", argv[i]);
        break;
      }
      bool white = true;
      for (int ii = ii0; ii < ii1; ++ii)
      {
        for (int c = 0; c < 3; ++c)
        {
          if (ibuffer[ii][c] < 0xFE)
          {
            white = false;
            if (! white)
            {
              break;
            }
          }
        }
        if (! white)
        {
          break;
        }
      }
      if (! started && white)
      {
        continue;
      }
      if (started && white && (ycount % SPEED) == 0)
      {
        break;
      }
      started = true;
      ycount++;
      if (scroll == 0)
      {
        if (0 <= y)
        {
#ifndef DRYRUN
          if (1 != fwrite(&oheader[0], oheaderlen, 1, stdout))
          {
            fprintf(stderr, "ERROR: could not write output\n");
          }
          if (1 != fwrite(&obuffer[y][0][0], obufferlen, 1, stdout))
          {
            fprintf(stderr, "ERROR: could not write output\n");
            return 1;
          }
#endif
          frames++;
        }
        y += SPEED;
        if (y == OH)
        {
          y = 0;
        }
      }
#ifndef DRYRUN
      memcpy(&obuffer[oj     ][0][0], &ibuffer[ii0][0], scanlinelen);
      memcpy(&obuffer[oj + OH][0][0], &ibuffer[ii0][0], scanlinelen);
#endif
      oj++;
      if (oj == OH)
      {
        oj = 0;
      }
      scroll++;
      if (scroll == SPEED)
      {
        scroll = 0;
      }
      if (i == argc && ycount == OH)
      {
        break;
      }
    }
    fclose(ifile);
  }
  fprintf(stderr, "NOTICE: %ld frames\n", frames);
  return 0;
}
