// rodney -- fractal zoom explorer bot
// Copyright (C) 2020,2021 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

#include <cassert>

#include "rodney.h"

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#else
#include "worker.h"
#endif

#ifdef RODNEY_SAVE_RAW

#include <ImfNamespace.h>
#include <ImfOutputFile.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfArray.h>
#include <ImfFrameBuffer.h>
namespace IMF = OPENEXR_IMF_NAMESPACE;
using namespace IMF;
#define save_raw true

void glsl_colour(FILE *o, const colour &c)
{
  std::fprintf(o, "vec3(%.7g,%.7g,%.7g)", c.v.v[0], c.v.v[1], c.v.v[2]);
}

void glsl_wave(FILE *o, const wave &w)
{
  std::fprintf(o, "wave(%.7g,%.7g,%d,vec3[MAX_COLOURS](", w.offset, w.period, w.ncolours);
  for (int i = 0; i < MAX_COLOURS; ++i)
  {
    if (i)
    {
      std::fprintf(o, ",");
    }
    glsl_colour(o, w.colours[i]);
  }
  std::fprintf(o, "))");
}

void glsl_multiwave(FILE *o, const multiwave &m)
{
  std::fprintf(o, "multiwave(%d,vec3(%.7g,%.7g,%.7g),float[MAX_WAVES](", m.nwaves, m.grey_point.v.v[0], m.grey_point.v.v[1], m.grey_point.v.v[2]);
  for (int i = 0; i < MAX_COLOURS; ++i)
  {
    if (i)
    {
      std::fprintf(o, ",");
    }
    std::fprintf(o, "%.7g", m.blend[i]);
  }
  std::fprintf(o, "),wave[MAX_WAVES](");
  for (int i = 0; i < MAX_COLOURS; ++i)
  {
    if (i)
    {
      std::fprintf(o, ",");
    }
    glsl_wave(o, m.waves[i]);
  }
  std::fprintf(o, "))");
}

void glsl_palette(FILE *o, const palette &p)
{
  std::fprintf(o, "palette(");
  glsl_multiwave(o, p.n);
  std::fprintf(o, ",");
  glsl_multiwave(o, p.s);
  std::fprintf(o, ",%.7g,%.7g,%.7g,%.7g,", p.blend_ns, p.slope_x, p.slope_y, p.slope_power);
  glsl_colour(o, p.slope_light);
  std::fprintf(o, ",");
  glsl_colour(o, p.slope_dark);
  std::fprintf(o, ",");
  glsl_colour(o, p.slope_grey);
  std::fprintf(o, ",%.7g,%d,%.7g,%s)", p.blend_slope, p.stripe_power, p.stripe_decay, p.parity ? "true" : "false");
}

#endif

static inline bool is_uniform(const image &img)
{
  unsigned char rgb[3] = { img.rgb[0], img.rgb[img.cstride], img.rgb[img.cstride * 2] };
  for (int j = 0; j < img.height; ++j)
  {
    for (int i = 0; i < img.width; ++i)
    {
      for (int c = 0; c < 3; ++c)
      {
        size_t ix = img.ystride * j + img.xstride * i + img.cstride * c;
        int d = int(img.rgb[ix]) - int(rgb[c]);
        if (d * d > 4)
        {
          return false;
        }
      }
    }
  }
  return true;
}

static inline void render_tile(image &img, const coordinates &co, const mat2 &skew, const formula &f, const int &maxiters0, const int &maxiters1, const int &aa, const palette &pal, uint32_t seed, int frame)
{
#ifdef RODNEY_SAVE_RAW
  // for storing raw data
  uint32_t *raw_n = nullptr;
  float *raw_nf = nullptr;
  float *raw_dex = nullptr;
  float *raw_dey = nullptr;
  float *raw_avg = nullptr;
  if (save_raw)
  {
    int count = img.height * aa * img.width * aa;
    raw_n = new uint32_t[count];
    raw_nf = new float[count];
    raw_dex = new float[count];
    raw_dey = new float[count];
    raw_avg = new float[count];
    std::memset(raw_n, 0xFF, count * sizeof(*raw_n));
    std::memset(raw_nf, 0, count * sizeof(*raw_n));
    std::memset(raw_dex, 0, count * sizeof(*raw_n));
    std::memset(raw_dey, 0, count * sizeof(*raw_n));
    std::memset(raw_avg, 0, count * sizeof(*raw_n));
  }
#else
  (void) seed;
  (void) frame;
#endif
  const double er = 25;
  const double er2 = er * er;
  const int stripe_power = pal.stripe_power;
  const double stripe_decay = pal.stripe_decay;
  const int h = img.height;
  const int w = img.width;
  const double s = 2 * co.r / h;
  const linearRGB black = { { { 0, 0, 0 } } };
  const linearRGB white = { { { 1, 1, 1 } } };
  for (int j = 0; j < h; ++j)
  {
    const int maxiters = maxiters0 + (maxiters1 - maxiters0) * j / h;
    for (int i = 0; i < w; ++i)
    {
      // linear light colour accumulator
      linearRGB acc = { { { 0, 0, 0 } } };
      for (int dj = 0; dj < aa; ++dj)
      {
        for (int di = 0; di < aa; ++di)
        {
          double u0 = i + (di + U(i * aa + di, j * aa + dj, 1)) / aa;
          double v0 = j + (dj + U(i * aa + di, j * aa + dj, 2)) / aa;
          double re = -0.6931471805599453 * v0 / h; // log 2
          double im = 6.283185307179586 * u0 / w; // 2 pi
          double R = 0.5 * std::hypot(w, h);
          double r = std::exp(re);
          double cosim = std::cos(im);
          double sinim = std::sin(im);
          u0 = R * r * cosim;
          v0 = R * r * sinim;
          vec2 u = { { u0, v0 } };
          vec2 v = skew * u * s;
          complex c =
            { dual(v.v[0] + co.cx, 0)
            , dual(v.v[1] + co.cy, 1)
          };
          complex z = c;
          double dex = 0.0;
          double dey = 0.0;
          double stripe = 0;
          double stripe1 = 0;
          int stanza = 0;
          int count = 0;
          int power = 2;
          linearRGB l = pal.parity ? black : white;
          for (int k = 1; k <= maxiters; ++k)
          {
            double nz = norm(z);
            complex w = pow(z / std::sqrt(nz), stripe_power);
            stripe1 = 0.5 + 0.5 * w.im.x;
            stripe *= stripe_decay;
            stripe += stripe1;
            if (nz > er2)
            {
              const vec2 u = { { z.re.x, z.im.x } };
              const mat2 J =
                { { { z.re.dx[0] * s, z.re.dx[1] * s }
                  , { z.im.dx[0] * s, z.im.dx[1] * s } } };
              const double num = length(u) * log(length(u));
              const vec2 den = normalize(u) * J * skew;
              const double m = num / norm(den) / r;
              dex =  m * den.v[0];
              dey = -m * den.v[1];
              if (! std::isfinite(dex) || ! std::isfinite(dey))
              {
                dex = 0.0;
                dey = 0.0;
              }
              double nf = std::log(std::log(nz) / std::log(er2))
                        / std::log(double(power));
              double n = k - nf;
              double de = std::hypot(dex, dey);
              float val = std::min(de, 1.0);
              double avg0 = stripe;
              double avg1 = (stripe - stripe1) / stripe_decay;
              double avg = avg1 + (1 - nf) * (avg0 - avg1);
              float desat =
                std::min(std::pow(de / img.width, 4), 1.0);
              colour c = palette_colour
                ( pal, 16 * n, 4 * avg
                , cosim * dex - sinim * dey
                , cosim * dey + sinim * dex
                );
              l = to_linearRGB(c);
              l.v = mix(l.v, pal.parity ? white.v : black.v, desat);
              l.v = mix(pal.parity ? black.v : white.v, l.v, val);
#ifdef RODNEY_SAVE_RAW
              // store raw data
              if (save_raw)
              {
                int ix = (j * aa + dj) * (img.width * aa) + (i * aa + di);
                raw_n[ix] = uint32_t(k);
                raw_nf[ix] = float(1.0 - nf);
                raw_dex[ix] = float(dex);
                raw_dey[ix] = float(dey);
                raw_avg[ix] = float(avg);
              }
#endif
              break;
            }
            // step formula
            if (++count >= f.group[stanza].repeats)
            {
              count = 0;
              if (++stanza >= f.ngroups)
              {
                stanza = f.loop_start;
              }
            }
            power = f.group[stanza].power;
            if (f.group[stanza].abs_x) z.re = abs(z.re);
            if (f.group[stanza].abs_y) z.im = abs(z.im);
            if (f.group[stanza].neg_x) z.re = -z.re;
            if (f.group[stanza].neg_y) z.im = -z.im;
            z = pow(z, power) + c;
          }
          acc.v = acc.v + l.v;
        }
      }
      acc.v = acc.v * (1.0 / (aa * aa));
      sRGB s = to_sRGB(acc);
      float sr = 255.0f * s.v.v[0] + U(i, j, 3);
      float sg = 255.0f * s.v.v[1] + U(i, j, 4);
      float sb = 255.0f * s.v.v[2] + U(i, j, 5);
      int ix = j * img.ystride + i * img.xstride;
      img.rgb[ix] = std::min(std::max(sr, 0.0f), 255.0f);
      ix += img.cstride;
      img.rgb[ix] = std::min(std::max(sg, 0.0f), 255.0f);
      ix += img.cstride;
      img.rgb[ix] = std::min(std::max(sb, 0.0f), 255.0f);
    }
  }
#ifdef RODNEY_SAVE_RAW
  if (save_raw)
  {
    char filename[1000];
    if (frame == 0)
    {
      std::snprintf(filename, 1000, "%08d-palette.frag", int(seed));
      FILE *o = std::fopen(filename, "wb");
      std::fprintf(o, "const palette pal = ");
      glsl_palette(o, pal);
      std::fprintf(o, ";\n");
      std::fclose(o);
    }
    std::snprintf(filename, 1000, "%08d_%04d_%.3e.exr", int(seed), int(frame), double(co.r));
    Header header(img.width * aa, img.height * aa);
    header.channels().insert("N",   Channel(IMF::UINT));
    header.channels().insert("NF",  Channel(IMF::FLOAT));
    header.channels().insert("T",   Channel(IMF::FLOAT));
    header.channels().insert("DEX", Channel(IMF::FLOAT));
    header.channels().insert("DEY", Channel(IMF::FLOAT));
    OutputFile of(filename, header);
    FrameBuffer fb;
    fb.insert("N",   Slice(IMF::UINT,  (char *) raw_n,   sizeof(*raw_n),   sizeof(*raw_n)   * img.width * aa));
    fb.insert("NF",  Slice(IMF::FLOAT, (char *) raw_nf,  sizeof(*raw_nf),  sizeof(*raw_nf)  * img.width * aa));
    fb.insert("T",   Slice(IMF::FLOAT, (char *) raw_avg, sizeof(*raw_avg), sizeof(*raw_avg) * img.width * aa));
    fb.insert("DEX", Slice(IMF::FLOAT, (char *) raw_dex, sizeof(*raw_dex), sizeof(*raw_dex) * img.width * aa));
    fb.insert("DEY", Slice(IMF::FLOAT, (char *) raw_dey, sizeof(*raw_dey), sizeof(*raw_dey) * img.width * aa));
    of.setFrameBuffer(fb);
    of.writePixels(img.height * aa);
    delete[] raw_n;
    delete[] raw_nf;
    delete[] raw_dex;
    delete[] raw_dey;
    delete[] raw_avg;
  }
#endif
}

extern render_output *render1(const render_input &in)
{
  int render_width = 1 << in.quality;
  int render_height = 1 << (in.quality - 3);
  int render_bytes = render_width * render_height * 3;
  image i = image_new(render_width, render_height);
  render_tile(i, in.coords, in.skew, in.f, in.maxiters0, in.maxiters1, in.aa, in.pal, in.seed, in.k);
  size_t bytes = sizeof(render_output) + render_bytes;
  render_output *out = (render_output *) std::malloc(bytes);
  std::memcpy(out->data, i.rgb, render_bytes);
  out->size = bytes;
  out->seed = in.seed;
  out->k = in.k;
  image_delete(i);
  if (in.verbose)
  {
    std::fprintf(stdout, "%u rendered tile at radius %g\n", in.seed, in.coords.r);
  }
  return out;
}

// web worker entry point

#ifdef __EMSCRIPTEN__

#ifdef RODNEY_WORKER

extern "C" void render(char *data, int size)
{
  assert(size == sizeof(render_input));
  render_input *in = (render_input *) data;
  try
  {
    render_output *out = render1(*in);
    emscripten_worker_respond((char *) out, out->size);
    std::free(out);
  }
  catch (const std::exception &e)
  {
    emscripten_worker_respond(0, 0);
  }
}

#endif

#else

struct work render(struct work job)
{
  assert(job.size == sizeof(render_input));
  render_input *in = (render_input *) job.data;
  try
  {
    render_output *out = render1(*in);
    struct work result;
    result.size = out->size;
    result.data = (char *) out;
    return result;
  }
  catch (const std::exception &e)
  {
    struct work result = { 0, 0 };
    return result;
  }
}

#endif
