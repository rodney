# rodney

autonomous fractal exploration bot

<https://mathr.co.uk/rodney>

<https://code.mathr.co.uk/rodney>

## about

In the early 1980s, industrial research super-computers were used to
generate images of the Mandelbrot set, the most famous mathematical
fractal. With the advances afforded by Moore's law, the 1 gigaflops of
the 1980s super-computer is now (in the early 2020s) left behind by
consumer hardware over 100 times faster.  Benoit B. Mandelbrot's ideas
about roughness at all scales being a relevant way to describe and
quantify shapes found in nature quickly found their way to the art
world.  With limitless visualizations of chaos and the infinite, the
question now becomes: where do you want to go today?  The fractal
explorer bot Rodney attempts to automate an answer, using mathematical
models to quantify fractal image aesthetics it navigates to places it
considers beautiful, and hopefully the humans watching may agree.

## flags

Flags can be added to the command line like `--flag argument`, or to the
URL like `?flag=argument`.

- `--antialiasing`, `--aa`: control number of samples per pixel.  AA 3
  will use 3^2 = 9 samples, taking 9 times more CPU power than AA 1.
  Image quality improves with higher AA levels.  Default is 3.
- `--explore-quality`: controls size of grid for exploration.  Default
  is 8.  Smaller will make it go faster at the risk of unreliability.
- `--render-quality`: controls size of rendered tiles for display.
  Default is 9.  Smaller will increase speed at the cost of repetition.
- `--seed`: control initial random seed.  Default is based on the time.
- `--threads`: override detected number of threads.  Increase if your
  system does not tell the truth, decrease if you want to reduce CPU
  load (may make animation less smooth).
- `--verbose`: control verbosity of messages.  Default is 0, mostly
  silent.  Adding 1 will print exploration logs, adding 2 will print
  rendering logs.

## todo

- adapt gracefully to window size changes during runtime (preserve pixel
  aspect ratio)
- ensure WebGL textures are sRGB
- investigate blurring vs crawling trade-off with linear filtering
- hide mouse cursor in animation mode if possible
- web form to enter options to be sent to the URL
- detect optimal iteration count via consistency of distance estimates
- improve performance of some of the algorithms
- probabalistic weighted choice of target in the explorer
- higher precision for deeper zooms (too slow, wait another decade...)

## changelog

### rodney-0.2

2021-05-01 "Go West Young Man", Dezoomber / FAB Co.Lab

Dezoomber 2020 generator extended for Co.Lab Sound at Fringe Arts Bath 2021.

online at:

- <https://mathr.co.uk/rodney/0.2/>
- <https://mathr.co.uk/dezoomber/>

changes include:

- autoskew using complex RMS for stretch direction
- multiwave colouring with stripe average + iteration count + directional
  DE for slope and distance + auto white balance
- integer hash PRNG for repeatable exploration, jitter, dither
- standalone POSIX version as well as WEB version
- uses EGL + GLES2 for GL via GLFW + glad for POSIX, via emscripten for WEB
- abstract threading using pthreads for POSIX and workers for WEB
- command line flags --explore-qualiy --render-quality --antialiasing
- raw EXR saving mode for export to zoomasm
- NRT sonification of exported videos implemented in OpenCL

### rodney-0.1

2020-10-28 "Big Brother", Generative Unfoldings prototype

online at:

<https://mathr.co.uk/rodney/prototype/>

submitted to:

MIT Center for Art, Science & Technology
Generative Unfoldings
Call for Sketches/Prototypes

"How can artist-programmers working in the field of
computer-generated art make unique contributions to
the current discussion of artificial intelligence (AI)?"

-- <https://arts.mit.edu/cast/symposia/generative-unfoldings-call/>

## legal

rodney -- autonomous fractal exploration bot

Copyright (C) 2020,2021 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
