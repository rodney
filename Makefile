## rodney -- fractal zoom explorer bot
## Copyright (C) 2020,2021 Claude Heiland-Allen
## SPDX-License-Identifier: AGPL-3.0-or-later

all: posix

posix: rodney rodney-exr sonify

web: rodney.js explore.js render.js

rodney: rodney.cc explore.cc render.cc worker_pthread.cc worker.h rodney.h glad/src/glad.c glad/include/glad/glad.h Makefile
	g++ -std=c++17 -Wall -Wextra -pedantic -O3 -o rodney -Iglad/include rodney.cc explore.cc render.cc worker_pthread.cc glad/src/glad.c -pthread -lGLESv2 -lglfw -g -ggdb

rodney-exr: rodney.cc explore.cc render.cc worker_pthread.cc worker.h rodney.h glad/src/glad.c glad/include/glad/glad.h Makefile
	g++ -std=c++17 -Wall -Wextra -pedantic -O3 -o rodney-exr -Iglad/include rodney.cc explore.cc render.cc worker_pthread.cc glad/src/glad.c -pthread -lGLESv2 -lglfw -g -ggdb `pkg-config --cflags --libs OpenEXR` -DRODNEY_SAVE_RAW

rodney.js: rodney.cc explore.cc render.cc worker_emscripten.cc worker.h rodney.h Makefile
	em++ -std=c++17 -Wall -Wextra -pedantic -O3 -Os -o rodney.html rodney.cc explore.cc render.cc worker_emscripten.cc -s MIN_WEBGL_VERSION=2 -s MAX_WEBGL_VERSION=2 -s USE_PTHREADS=0 -s ALLOW_MEMORY_GROWTH=1
	gzip -9 -k -f rodney.js
	gzip -9 -k -f rodney.wasm
	gzip -9 -k -f index.html

explore.js: explore.cc rodney.h Makefile
	em++ -std=c++17 -Wall -Wextra -pedantic -O3 -Os -o explore.js explore.cc -s EXPORTED_FUNCTIONS="['_explore']" -s BUILD_AS_WORKER=1 -s USE_PTHREADS=0 -s ALLOW_MEMORY_GROWTH=1 -DRODNEY_WORKER=1
	gzip -9 -k -f explore.js
	gzip -9 -k -f explore.wasm

render.js: render.cc rodney.h Makefile
	em++ -std=c++17 -Wall -Wextra -pedantic -O3 -Os -o render.js render.cc -s EXPORTED_FUNCTIONS="['_render']" -s BUILD_AS_WORKER=1 -s USE_PTHREADS=0 -s ALLOW_MEMORY_GROWTH=1 -DRODNEY_WORKER=1
	gzip -9 -k -f render.js
	gzip -9 -k -f render.wasm

sonify: sonify.c sonify.h Makefile
	gcc -std=c99 -Wall -Wextra -pedantic -O3 -march=native -fopenmp -o sonify sonify.c -lm -lsndfile -lOpenCL -Wno-overlength-strings -Wno-deprecated-declarations

sonify.h: s2c.sh sonify.cl Makefile
	./s2c.sh opencl < sonify.cl > sonify.h

.PHONY: all posix web
