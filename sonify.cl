// rodney -- fractal zoom explorer bot
// Copyright (C) 2021 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-or-later

#define twopi 6.283185307179586f
#define SR 44100
#define FPS 30
#define BLOCK (SR/FPS)
#define BF 2

typedef float sample;
typedef struct { sample buf[2][BLOCK/BF]; } DELAY20ms;
typedef struct { sample w[2][2][2]; } HILBERT;
typedef struct { sample re, im; } VCF;
typedef struct { sample y; } LOP;
struct pixel
{
  DELAY20ms del;
  HILBERT wave;
  VCF filter;
  LOP hz;
  LOP q;
  sample h_target;
  sample s_target;
};


// based on pd's [vcf~] [lop~] [hip~]

sample vcf(VCF *s, sample x, sample hz, sample q)
{
  sample qinv = q > 0 ? 1 / q : 0;
  sample ampcorrect = 2 - 2 / (q + 2);
  sample cf = hz * twopi / SR;
  if (cf < 0) { cf = 0; }
  sample r = qinv > 0 ? 1 - cf * qinv : 0;
  if (r < 0) { r = 0; }
  sample oneminusr = 1 - r;
  sample cre = r * cos(cf);
  sample cim = r * sin(cf);
  sample re2 = s->re;
  s->re = ampcorrect * oneminusr * x + cre * re2 - cim * s->im;
  s->im = cim * re2 + cre * s->im;
  return s->re;
}

sample lop(LOP *s, sample x, sample hz) {
  sample c = fmin(fmax(twopi * hz / SR, 0), 1);
  return s->y = x * c + s->y * (1 - c);
}

// pd/extra/hilbert~.pd

void hilbert(sample out[2], HILBERT *h, const sample in[2]) {
  const sample c[2][2][5] =
    { { {  1.94632, -0.94657,   0.94657,  -1.94632, 1 }
      , {  0.83774, -0.06338,   0.06338,  -0.83774, 1 }
      }
    , { { -0.02569,  0.260502, -0.260502,  0.02569, 1 }
      , {  1.8685,  -0.870686,  0.870686, -1.8685,  1 }
      }
    };
  for (int i = 0; i < 2; ++i) {
    sample w = in[i] + c[i][0][0] * h->w[i][0][0] + c[i][0][1] * h->w[i][0][1];
    sample x = c[i][0][2] * w + c[i][0][3] * h->w[i][0][0] + c[i][0][4] * h->w[i][0][1];
    h->w[i][0][1] = h->w[i][0][0];
    h->w[i][0][0] = w;
    w = x + c[i][1][0] * h->w[i][1][0] + c[i][1][1] * h->w[i][1][1];
    out[i] = c[i][1][2] * w + c[i][1][3] * h->w[i][1][0] + c[i][1][4] * h->w[i][1][1];
    h->w[i][1][1] = h->w[i][1][0];
    h->w[i][1][0] = w;
  }
}

uint hash(uint a)
{
    a = (a+0x7ed55d16u) + (a<<12u);
    a = (a^0xc761c23cu) ^ (a>>19u);
    a = (a+0x165667b1u) + (a<<5u);
    a = (a+0xd3a2646cu) ^ (a<<9u);
    a = (a+0xfd7046c5u) + (a<<3u);
    a = (a^0xb55a4f09u) ^ (a>>16u);
    return a;
}

__kernel void analyze_input
( __global const unsigned char *input // image
, __global        struct pixel *state // state
,                        int    width
,                        uint   seed
)
{
  int j = get_global_id(0);
  int i = get_global_id(1);
  int ix = (j * width + i) * 3;
  int ox = j * width + i;
  sample r = (input[ix + 0] + hash(ix + 0 + seed) / (sample)(1LL << 32)) / 256.0f;
  sample g = (input[ix + 1] + hash(ix + 1 + seed) / (sample)(1LL << 32)) / 256.0f;
  sample b = (input[ix + 2] + hash(ix + 2 + seed) / (sample)(1LL << 32)) / 256.0f;
  sample c[3] = { r, g, b };
  sample K[4] = { 0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0 };
  sample p[4] =
    { c[1] < c[2] ? c[2] : c[1]
    , c[1] < c[2] ? c[1] : c[2]
    , c[1] < c[2] ? K[3] : K[0]
    , c[1] < c[2] ? K[2] : K[1]
    };
  sample q[4] =
    { c[0] < p[0] ? p[0] : c[0]
    ,               p[1]
    , c[0] < p[0] ? p[3] : p[2]
    , c[0] < p[0] ? c[0] : p[0]
    };
  sample d = q[0] - fmin(q[3], q[1]);
  sample e = 1.0e-10;
  sample h = fabs(q[2] + (q[3] - q[1]) / (6 * d + e));
  sample s = d / (q[0] + e);
  sample v = q[0];
  if (isnan(h) || isinf(h)) h = 0;
  if (isnan(s) || isinf(s)) s = 0;
  if (isnan(v) || isinf(v)) v = 0;
  state[ox].h_target = h;
  state[ox].s_target = s * v;
}

__kernel void generate_audio
( __global        struct pixel *state
, __global              sample *buffer
,                          int  width
,                          int  height
,                          int  count
,                          int  dr
,                          uint seed
)
{
  int j = get_global_id(0);
  int i = get_global_id(1);
  int ix = j * width + i;
  int ox = (j * width + i) * count;
  int ixim = j * width + (i-1);
  int ixip = j * width + (i+1);
  int ixjm = (j-1) * width + i;
  int ixjp = (j+1) * width + i;
  int dw = ! dr;
  VCF filter = state[ix].filter;
  HILBERT wave = state[ix].wave;
  LOP lops = state[ix].q;
  LOP loph = state[ix].hz;
  sample s_target = state[ix].s_target;
  sample h_target = state[ix].h_target;
  for (int k = 0; k < count; ++k)
  {
    sample noise = (hash(seed + k) / (sample)(1LL << 32) - 0.5f) * 1e-6;
    sample d = - (noise +
      (i ==        0 ? 0 : state[ixim].del.buf[dr][k]) +
      (i ==  width-1 ? 0 : state[ixip].del.buf[dr][k]) +
      (j ==        0 ? 0 : state[ixjm].del.buf[dr][k]) +
      (j == height-1 ? 0 : state[ixjp].del.buf[dr][k]) +
                           state[ix  ].del.buf[dr][k] * 4) / 8;
    sample s = lop(&lops, s_target, 10);
    sample h = lop(&loph, h_target, 10);
    sample Q = sqrt(0.5f) + pow(1000.0f, s);
    sample f = 100.0f * pow(4.0f, h);
    sample v = vcf(&filter, sqrt(s) * d, f, Q);
    // band-limited sawtooth wave, 2 * 200Hz * 16 -> 6400Hz < Nyquist limit
    sample b[2] = { v, v };
    hilbert(b, &wave, b);
    sample a = hypot(b[0], b[1]);
    if (! (a > 0)) a = 1;
    sample x = b[1] / a;
    sample y = b[0] / a;
    sample U0 = 1;
    sample U1 = 2 * x;
    sample w = 1;
    for (int n = 2; n < 16; ++n)
    {
      w = -w;
      w += U1 / n;
      sample U2 = 2 * x * U1 - U0;
      U0 = U1;
      U1 = U2;
    }
    sample p = a * y * w;
    if (isnan(p) || isinf(p)) p = 0;
    state[ix].del.buf[dw][k] = tanh(5 * p);
    buffer[ox + k] = p;
  }
  state[ix].filter = filter;
  state[ix].wave = wave;
  state[ix].q = lops;
  state[ix].hz = loph;
}

__kernel void accumulate_output
( __global        const sample *gain
, __global        const sample *buffer
, __global              sample *output
,                          int  width
,                          int  height
,                          int  count
)
{ 
  int k = get_global_id(0);
  sample sum[2] = { 0, 0 };
  for (int i = 0; i < width; ++i)
  {
    sample column = 0;
    for (int j = 0; j < height; ++j)
    {
      int ix = (j * width + i) * count + k;
      column += buffer[ix];
    }
    sum[0] += gain[i * 2 + 0] * column;
    sum[1] += gain[i * 2 + 1] * column;
  }
  output[k * 2 + 0] = sum[0];
  output[k * 2 + 1] = sum[1];
}
